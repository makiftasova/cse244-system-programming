/* 
 * File:   FIFOLUls.c
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * A little modified clone of well-known ls with FIFO
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/stat.h>

#include "FIFOLUls.h"

int main(int argc, CHAR_PTR argv[])
{
	/* The directory path where program begins */
	CHAR_PTR cptrBeginDir = NULL;

	CHAR_PTR cptrCurrDir = NULL; /* cwd for listing ingredients */

	/* maximum  possible name length of a pathname */
	long long int lMaxPathNameLen = 0;

	char cTmpBuff = 0;

	/* Path to FIFO file */
	CHAR_PTR cptrFifoFile = NULL;
	/* File Descriptor of FIFO file */
	FILE_DES fdFifo = 0;

	const pid_t pidParent = getpid();
	
	int iJunkLen = 0;

	if ((argc < 1) || (argc > 2))
		return fnUsage(argv[0]);

	cptrBeginDir = ((1 == argc) ? CURRENT_DIR : argv[1]);

	if (FAIL == (lMaxPathNameLen = pathconf(CURRENT_DIR, _PC_PATH_MAX))) {
		perror("Failed to determine maximum length of path\n");
		return(EXIT_FAIL_DETERMINE_PATH_LEN);
	}

	if (!(cptrCurrDir = (CHAR_PTR) malloc(lMaxPathNameLen))) {
		perror("Failed to allocate memory for storing pathname\n");
		return(EXIT_PATHNAME_ALLOC_FAIL);
	}

	if ((FAIL == (chdir(cptrBeginDir))) && (errno == EACCES)) {
		fnPrintPermDndErr(argv[0], cptrBeginDir);
		return(EXIT_PERM_DENIED);
	}

	if (!(getcwd(cptrCurrDir, lMaxPathNameLen))) {
		perror("Failed to get current working directory\n");
		return(EXIT_GETCWD_FAIL);
	}

	cptrFifoFile = (CHAR_PTR) calloc(sizeof(char),
		(strlen(FIFO_FILE) + 10));

	sprintf(cptrFifoFile, "%s-%d", FIFO_FILE, pidParent);

	/* create FIFO */
	mkfifo(cptrFifoFile, 0666);

	if (fork() != 0) {
		iJunkLen = fnPrintBeginPrint(cptrCurrDir);

		fdFifo = open(cptrFifoFile, O_RDONLY);

		while ((read(fdFifo, &cTmpBuff, sizeof(char))))
			printf("%c", cTmpBuff);

		close(fdFifo);

		while (0 < fnWaitChilds(NULL));

		/* Unlink FIFO */
		unlink(cptrFifoFile);
		
		fnEndPrint(iJunkLen);

	} else {

		if ((fdFifo = open(cptrFifoFile, O_WRONLY)) < 1) {
			perror("Failed to create FIFO");
			return(EXIT_FIFO_FAIL);
		}
		fnListDir(cptrCurrDir, argv[0], fdFifo);

		while (0 < fnWaitChilds(NULL));

		close(fdFifo);
	}

	return(EXIT_SUCCESS);
}

int fnUsage(const CHAR_PTR cptrExecName)
{
	printf("usage:\n------\n%s [DIRPATH]\n", cptrExecName);
	return(EXIT_USAGE_PRINT);
}

BOOL_T fnIsDirectory(const CHAR_PTR ccptrPath)
{
	STAT stStatBuffer;

	if (FAIL == stat(ccptrPath, &stStatBuffer)) {
		return FALSE;
	}
	return S_ISDIR(stStatBuffer.st_mode);
}

int fnPrintBeginPrint(const CHAR_PTR cptrPath)
{
	int iCount = 0; /* count of printed chars by printf */
	int iCntr = 0; /* generic loop counter */

	iCount = printf("Starting to list from directory %s\n", cptrPath);
	for (iCntr = 0; iCntr < iCount; ++iCntr)
		putchar('-');
	putchar('\n');

	return iCount;
}

void fnEndPrint(const int iLineWidth)
{
	int iMidint = ((iLineWidth / 2) - 5);
	int iCntr = 0; /* generic counter */

	for (iCntr = 0; iCntr < iLineWidth; ++iCntr)
		putchar('-');
	putchar('\n');
	for (iCntr = 0; iCntr < iMidint; ++iCntr)
		putchar(' ');
	printf("End of list\n");
	return;
}

void fnPrintPermDndErr(const CHAR_PTR cptrExecName, const CHAR_PTR cptrPath)
{
	CHAR_PTR cptrErrMsg = NULL;
	int iLen = 0;

	iLen = strlen(cptrExecName) + strlen(cptrPath) + 44;
	cptrErrMsg = (CHAR_PTR) malloc(sizeof(char) * iLen);

	sprintf(cptrErrMsg, "%s%s%s", cptrExecName, ": cannot open directory ",
		cptrPath);

	perror(cptrErrMsg);
	free(cptrErrMsg);

	return;
}

int fnListDir(const CHAR_PTR cptrPathToList, const CHAR_PTR cptrExecName,
	FILE_DES fdFifo)
{
	DIRENT_PTR deptrDirent = NULL;
	DIR_PTR dptrDir = NULL; /* pointer to current dir */
	CHAR_PTR cptrNewPath = NULL;

	int iAllocLen = 0; /* Length of subdir's path name string */
	int iCntr1 = 0; /* generic counters */

	CHAR_PTR cptrWriteStr = NULL;
	UNSIG_INT uWriteStrLen = 0;

	if (!(dptrDir = opendir(cptrPathToList))) {
		perror("Failed to open directory\n");
		printf("dir: %s\n", cptrPathToList);
		exit(FAIL);
	}

	while ((deptrDirent = readdir(dptrDir))) {
		if ((strcmp(deptrDirent->d_name, CURRENT_DIR)) &&
			(strcmp(deptrDirent->d_name, PARENT_DIR))) {

			/* prepare FIFO write buffer */
			uWriteStrLen = strlen(cptrPathToList);
			uWriteStrLen += strlen(deptrDirent->d_name);
			uWriteStrLen += 15;

			cptrWriteStr = (CHAR_PTR) calloc(sizeof(char),
				uWriteStrLen);

			sprintf(cptrWriteStr, "%d%5c%-1s/%s\n", getpid(), ' ',
				cptrPathToList, deptrDirent->d_name);

			/* Write info to FIFO */
			write(fdFifo, cptrWriteStr,
				sizeof(char) * strlen(cptrWriteStr));

			if ((fnIsDirectory(deptrDirent->d_name)) &&
				(strcmp(deptrDirent->d_name, cptrPathToList))) {

				iAllocLen = strlen(cptrPathToList) +
					strlen(deptrDirent->d_name) + 1;

				if (!(cptrNewPath = (CHAR_PTR) malloc
					(sizeof(char) * (iAllocLen + 1)))) {


					perror("Failed to allocate memory "
						"for new path name\n");
					exit(FAIL);
				}

				strncpy(cptrNewPath, cptrPathToList,
					iCntr1 = strlen(cptrPathToList));

				if (cptrNewPath[iCntr1 - 1] != '/') {
					cptrNewPath[iCntr1] = '/';
					++iCntr1;
				}

				strncpy(cptrNewPath + iCntr1,
					deptrDirent->d_name,
					strlen(deptrDirent->d_name));

				cptrNewPath[iAllocLen] = '\0';

				if (fork() == 0) {

					if ((FAIL == (chdir(cptrNewPath)))
						&& (errno == EACCES)) {

						fnPrintPermDndErr(cptrExecName,
							cptrNewPath);
						exit(0);

					} else {

						fnListDir(cptrNewPath,
							cptrExecName, fdFifo);

						exit(0);
					}

				} else {
					while (0 < fnWaitChilds(NULL));
				}

				free(cptrNewPath);
				chdir(cptrPathToList);
			}
		}
	}

	while ((closedir(dptrDir) == FAIL) && (errno == EINTR));

	/*return(iNumOfSubDirs);*/
	exit(EXIT_SUCCESS);
}

pid_t fnWaitChilds(INT_PTR iptrStatLoc)
{
	int iRetval;

	while (((iRetval = wait(iptrStatLoc)) == -1) && (errno == EINTR));
	return iRetval;
}

/* End of FIFOLUls.c.c */
