/* 
 * File:   lsthread.h
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 *  A clone of lsfork which uses threads instead of processes
 *
 *  Lists ingredients of given directory and all of its sub-directories
 *
 */

#ifndef LSTHREAD_H
#define LSTHREAD_H

#include <stdio.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>

/* Exit code which will return by usage function*/
#define EXIT_USAGE_PRINT 1

/* A little string which always points cwd when used as dir path */
#define CURRENT_DIR "."

/* 
 * A little string which always points parent dir of cwd
 *  when used as dir path 
 */
#define PARENT_DIR ".."

/* Some useful defines for using with system function */
#define FAIL -1

/* typedef for File Descriptor */
typedef int FILE_DES;

/*
 * A struct for passing arguments of fnListDir() to another thread since
 *  ptherad_create() only accepts one argument for arguments of start_routine
 */
typedef struct {
	char strPathToList[BUFSIZ];
	FILE_DES fdFileDes;
} ARGUMENT_T;

/* typedef for standard C identifiers */
typedef void* VOID_PTR;

typedef char* CHAR_PTR;

typedef ARGUMENT_T* ARGUMENT_PTR;

/* typedefs for types from <dirent.h> */
typedef DIR* DIR_PTR;

typedef struct dirent DIRENT;

typedef DIRENT* DIRENT_PTR;

/* typedefs for types from <sys/stat.h> */
typedef struct stat STAT;

/*
 * Prints usage of program by given executable name 
 *  (in most situations this is argv[0]).
 * 
 * cptrExecName - Current name of programs executable name.
 */
int fnUsage(const CHAR_PTR cptrExecName);

/*
 * Takes a file path, then checks is given file is a directory or not. Returns
 *  0 if given path is not a directory
 * 
 * ccptrPath - File path to check.
 */
int fnIsDirectory(const CHAR_PTR cptrPath);

/*
 * Prints given path to stdout. only useful for beginning of program.
 * 
 * cptrPath - Path to print.
 */
int fnPrintBeginPrint(const CHAR_PTR cptrPath);

/*
 * Prints end of list message to stdout. only useful for end of program.
 * 
 * iLineWidth - Line width value from fnPrintBeginPrint function
 */
void fnEndPrint(const int iLineWidth);

/*
 * Lists directories and files in given directory path if possible. If fails to
 *  open directory returns FAIL(-1)
 *
 *  args - an ARGUMENT_T object which contains a Path and a File Descriptor as
 *   output stream
 * 
 */
VOID_PTR fnListDir(const VOID_PTR args);

#endif

/* End of lsthread.h */
