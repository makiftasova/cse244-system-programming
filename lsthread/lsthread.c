/* 
 * File:   lsthread.c
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * A clone of lsfork which uses threads instead of processes
 * 
 * Lists ingredients of given directory and all of its sub-directories
 *
 */

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>

#include "lsthread.h"

int main(int argc, CHAR_PTR argv[]) {

	ARGUMENT_T args;
	int iLineLen = 0;
	pthread_t threadNew;

	if (argc != 2) {
		switch (argc) {
		case 1:
			printf("Too few arguments given\n");
			break;
		default:
			printf("Too many arguments given\n");
			break;
		}
		return fnUsage(argv[0]);
	}

	if (argv[1][0] == '-') {
		if (!strcmp(argv[1], "-h")) {
			return fnUsage(argv[0]);
		} else if (!strcmp(argv[1], "--help")) {
			return fnUsage(argv[0]);
		} else {
			printf("There is no such option: %s\n", argv[1]);
			return fnUsage(argv[0]);
		}
	}

	if (!fnIsDirectory(argv[1])) {
		printf("ERR: %s is not a directory\n", argv[1]);
		return fnUsage(argv[0]);
	}

	memset(args.strPathToList, '\0', BUFSIZ * sizeof(char));
	sprintf(args.strPathToList, "%s", argv[1]);
	args.fdFileDes = STDOUT_FILENO;

	iLineLen = fnPrintBeginPrint(args.strPathToList);

	if (pthread_create(&threadNew, NULL, fnListDir, (VOID_PTR) &args) != 0) {
		printf("%lu  |  #ERR# Failed to create thread for path: \"%s\". %s\n",
				pthread_self(), args.strPathToList,
				"Skipping Directory. #ERR#");

	} else {
		pthread_join(threadNew, NULL );
	}

	fnEndPrint(iLineLen);

	return (EXIT_SUCCESS);
}

int fnUsage(const CHAR_PTR cptrExecName) {
	printf("\n");
	printf("Usage: %s [OPTION]\n", cptrExecName);
	printf("  Or:  %s [DIRECTORY]\n", cptrExecName);
	printf("Lists ingredients of given directory and all ");
	printf("of its sub-directories\n");
	printf("\nDIRECTORY - Directory path to list.\n");
	printf("\nOPTION could be:\n");
	printf("-h or --help - Prints this help message.\n");
	return (EXIT_USAGE_PRINT);
}

int fnIsDirectory(const CHAR_PTR ccptrPath) {
	STAT stStatBuffer;

	if (FAIL == stat(ccptrPath, &stStatBuffer)) {
		return 0;
	}
	return S_ISDIR(stStatBuffer.st_mode);
}

int fnPrintBeginPrint(const CHAR_PTR cptrPath) {
	int iCount = 0;
	int iHead = 0;
	int iCntr = 0;
	int iReturn = 0;

	printf("Program started with PID: %d\n\n", getpid());

	iCount = printf("Starting to list from directory %s\n", cptrPath);

	iHead = printf("\n%3cThread ID%8c%18s\n", ' ', ' ', "FILE");

	iReturn = ((iCount > iHead) ? iCount : (iHead + 18));

	for (iCntr = 0; iCntr < iReturn; ++iCntr)
		putchar('-');
	putchar('\n');

	return iReturn;
}

void fnEndPrint(const int iLineWidth) {
	int iMidint = ((iLineWidth / 2) - 5);
	int iCntr = 0; /* generic counter */

	for (iCntr = 0; iCntr < iLineWidth; ++iCntr)
		putchar('-');
	putchar('\n');
	for (iCntr = 0; iCntr < iMidint; ++iCntr)
		putchar(' ');
	printf("End of list\n");
	return;
}

VOID_PTR fnListDir(const VOID_PTR args) {

	DIRENT_PTR deptrDirent = NULL;
	DIR_PTR dptrDir = NULL; /* pointer to current dir */
	/*char strNewPath[BUFSIZ];*/

	int iNumOfSubDirs = 0; /* number of sub-directories of cwd */

	char strPrintLine[BUFSIZ];
	int iPrintLine = 0;
	char strMsg[BUFSIZ];

	ARGUMENT_T argCurr;
	ARGUMENT_T argNew;

	ARGUMENT_PTR argPtr;

	pthread_t threadNew;

	memset(strPrintLine, '\0', BUFSIZ * sizeof(char));
	memset(strMsg, '\0', BUFSIZ * sizeof(char));
	memset(argNew.strPathToList, '\0', BUFSIZ * sizeof(char));

	argPtr = (ARGUMENT_PTR) args;
	argCurr = (*argPtr);

	if (!(dptrDir = opendir(argCurr.strPathToList))) {
		sprintf(strMsg,
				"%lu%2c|%2c#WARN# Cannot open directory: %s: %s #WARN#\n",
				pthread_self(), ' ', ' ', argCurr.strPathToList,
				strerror(errno));
		write(argCurr.fdFileDes, strMsg, (sizeof(char) * strlen(strMsg)));
		memset(strMsg, '\0', BUFSIZ * sizeof(char));
		return (NULL );
	}

	while ((deptrDirent = readdir(dptrDir))) {
		if ((strcmp(deptrDirent->d_name, CURRENT_DIR))
				&& (strcmp(deptrDirent->d_name, PARENT_DIR))) {

			if (argCurr.strPathToList[strlen(argCurr.strPathToList) - 1]
					!= '/') {
				strcat(argCurr.strPathToList, "/");
			}

			/* Write found files to FIFO */
			sprintf(strPrintLine, "%lu%2c|%2c%s%s\n", pthread_self(), ' ', ' ',
					argCurr.strPathToList, deptrDirent->d_name);
			iPrintLine = (strlen(strPrintLine) * sizeof(char));
			write(argCurr.fdFileDes, strPrintLine, iPrintLine);
			memset(strPrintLine, '\0', BUFSIZ * sizeof(char));

			/* New Path Begin */
			memset(argNew.strPathToList, '\0', BUFSIZ * sizeof(char));

			sprintf(argNew.strPathToList, "%s%s", argCurr.strPathToList,
					deptrDirent->d_name);

			if (fnIsDirectory(argNew.strPathToList)) {
				++iNumOfSubDirs;

				if ((FAIL == (chdir(argNew.strPathToList)))
						&& (errno == EACCES)) {
					/* Permission denied error */
					sprintf(strMsg,
							"%lu%2c|%2c#WARN# Cannot open directory: %s: %s #WARN#\n",
							pthread_self(), ' ', ' ', argNew.strPathToList,
							strerror(errno));
					write(argCurr.fdFileDes, strMsg,
							(sizeof(char) * strlen(strMsg)));
					memset(strMsg, '\0', BUFSIZ * sizeof(char));

				} else {
					argNew.fdFileDes = argCurr.fdFileDes;

					if (pthread_create(&threadNew, NULL, fnListDir,
							(VOID_PTR) &argNew) != 0) {

						printf("%lu  |  %s \"%s\". %s\n", pthread_self(),
								"#ERR# Failed to create thread for path:",
								argNew.strPathToList,
								"Skipping Directory. #ERR#");

					} else {
						pthread_join(threadNew, NULL );
					}

				}

			} else {
				continue;
			}

		}
	}

	while ((closedir(dptrDir) == FAIL) && (errno == EINTR))
		;

	return (NULL );
}

/* End of lsthread.c */
