/* 
 * File:   lsfork.h
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * CSE244 - HW02 - Part02
 * 
 * A Forked version of ls
 * 
 */



#ifndef MAT_LSFORK
#define MAT_LSFORK

#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>

/* Exit code which will return by usage function*/
#define EXIT_USAGE_PRINT 1

/* Exit code for failing to open directory */
#define EXIT_PERM_DENIED 2

/* Exit code for failed to determine maximum length of path */
#define EXIT_FAIL_DETERMINE_PATH_LEN 3

/* Exit code for failed to allocate memory for storing pathname */
#define EXIT_PATHNAME_ALLOC_FAIL 4

/* Exit code for failed to get current working directory */
#define EXIT_GETCWD_FAIL 5

/* A little string which always points cwd when used as dir path */
#define CURRENT_DIR "."

/* 
 * A little string which always points parent dir of cwd
 *  when used as dir path 
 */
#define PARENT_DIR ".."

/*
 * Output buffer for output sharing between child and parent proccess
 */
#define STD_OUT_BUFF "/tmp/outbuff"

/* Some useful defines for using with system function */
#define FAIL -1
#define SUCCESS 0

/* Type definiton from boolean type */
typedef enum {
    FALSE = 0, TRUE = 1
} BOOL_T;

/* typedef for standard C identifiers */
typedef char* CHAR_PTR;

typedef int* INT_PTR;

/* typedef for FILE pointer */
typedef FILE* FILE_PTR;

/* typedefs for types from dirent.h */
typedef DIR* DIR_PTR;

typedef struct dirent DIRENT;

typedef DIRENT* DIRENT_PTR;

typedef DIRENT** DIRENT_PTRPTR;

/* typedefs for types from sys/stat.h */
typedef struct stat STAT;


/**
 * Prints usage of program by given executable name 
 *  (in most situations this is argv[0]).
 * 
 * @param cptrExecName Current name of programs executable name.
 * @return A code which can be used as exit code.
 */
int fnUsage(const CHAR_PTR ccptrExecName);

/**
 * Takes a file path, then checks is given file is a directory or not.
 * 
 * @param ccptrPath File path to check.
 * @return ture(1) if given file is a directory else false(0).
 */
BOOL_T fnIsDirectory(const CHAR_PTR ccptrPath);

/**
 * Prints given path to stdout. only useful for beginning of program.
 * 
 * @param cptrPath Path to print.
 */
int fnPrintBeginPrint(const CHAR_PTR cptrPath);

/**
 * Lists directories and files in given directory path if possible. If fails to
 *  open directory returns FAIL(-1)
 * 
 * @param cptrPathToList Path to list ingredients.
 * @param cptrExecName Executable name of program
 * @param fptrParentBuff File buffer of parent
 * @return Number of directories in given directory. If fails to open 
 * directory returns FAIL(-1)
 */
int fnListDir(const CHAR_PTR cptrPathToList, const CHAR_PTR cptrExecName);
/*int fnListDir(const CHAR_PTR cptrPathToList, const CHAR_PTR cptrExecName, const FILE_PTR fptrParentBuff);*/

/**
 * Prints end of list message to stdout. only useful for end of program.
 * @param iLineWidth Line width value from fnPrintBeginPrint function
 */
void fnEndPrint(const int iLineWidth);

/**
 * Prints error message with perror. Message is "[cptrExecName] cannot open 
 * directory [cptrPath]: Permission denied".
 * 
 * @param cptrExecName Executable name of program.
 * @param cptrPath Path to denied permission.
 */
void fnPrintPermDeniedErr(const CHAR_PTR cptrExecName, const CHAR_PTR cptrPath);

/**
 * Function for waiting all child proccess to terminate
 */
pid_t fnWaitChilds(INT_PTR iptrStatLoc);

#endif

/* End of lsfork.h */
