/*
 * tellmeMORE.c
 *
 *  Created on: Apr 21, 2013
 *      Author: Mehmet Akif TAŞOVA
 *      Student Number: 111044016
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <time.h>
#include <ctype.h>
#include <signal.h>

#include "tellmeMORE.h"

TIMEVAL tBegin;

FILEDES fdServerClient;
FILEDES fdClientServer;

char strServerClientFifo[PATH_MAX];

FILEDES fdLogFile;
char strLogFile[S_LOG_NAME_MAX];

pid_t pidChilds[CHILD_MAX];
CHILD_T chChilds;
int iNumOfChilds;

TERMIOS_T tOrigin;
pid_t pidOrigin;

int main(int argc, CHAR_PTR argv[]) {

	char strClientServerFifo[PATH_MAX];
	char strShared[BUFSIZ];
	char strWorkingDir[PATH_MAX];
	char strUserCommand[BUFSIZ];
	char strTmp[6];
	char strCommand[CMD_MAX];
	char strArgument[BUFSIZ];
	char strFileName[BUFSIZ];
	char strLogMsg[BUFSIZ];

	char cTmp = '1';

	FILE_PTR fptrListFile = NULL;

	pid_t pidChild = 0;

	int iCntr = 0;

	time_t tRawFormat;

	pidOrigin = getpid();

	tcgetattr(STDIN_FILENO, &tOrigin);

	iNumOfChilds = 0;

	memset(strClientServerFifo, '\0', PATH_MAX * sizeof(char));
	memset(strShared, '\0', BUFSIZ * sizeof(char));
	memset(strWorkingDir, '\0', PATH_MAX * sizeof(char));
	memset(strUserCommand, '\0', BUFSIZ * sizeof(char));
	memset(strTmp, '\0', 6 * sizeof(char));
	memset(strCommand, '\0', CMD_MAX * sizeof(char));
	memset(strArgument, '\0', BUFSIZ * sizeof(char));
	memset(strFileName, '\0', BUFSIZ * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	if (argc != 2) {
		return fnUsage(argv[0]);
	}

	signal(SIGINT, fnSigIntHandler);
	signal(SIGUSR1, fnSigUsr1Handler);

	sprintf(strLogFile, "%s%d%s", S_TMP_LOG, getpid(), ".log");
	fdLogFile = open(strLogFile, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);

	sprintf(strLogMsg, "INFO: Starting tellmeMORE...\n");
	write(fdLogFile, strLogMsg, (strlen(strLogMsg) * sizeof(char)));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	time(&tRawFormat);
	sprintf(strLogMsg, "INFO: Starting Time: %s", ctime(&tRawFormat));
	write(fdLogFile, strLogMsg, (strlen(strLogMsg) * sizeof(char)));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	while (1) {

		memset(strUserCommand, '\0', PATH_MAX * sizeof(char));
		memset(strCommand, '\0', CMD_MAX * sizeof(char));
		memset(strArgument, '\0', BUFSIZ * sizeof(char));

		printf(" => ");
		/*gets(strUserCommand);*/
		fgets(strUserCommand, BUFSIZ, stdin);
		strtok(strUserCommand, "\n");

		sprintf(strLogMsg, "INFO: User Command: %s -- Time: %s", strUserCommand,
				ctime(&tRawFormat));
		write(fdLogFile, strLogMsg, (strlen(strLogMsg) * sizeof(char)));
		memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

		strncpy(strTmp, strUserCommand, 5);
		strTmp[5] = CHAR_NULL;
		strtok(strTmp, " \n\t\b");

		if ((strUserCommand[4] != CHAR_SPACE)
				&& (strUserCommand[4] != CHAR_NULL)) {
			printf("Unrecognized command: %s\n", strUserCommand);
			fnHelp();
			printf("\n");
			continue;

		} else if (strlen(strTmp) < 4) {
			printf("Unrecognized command: %s\n", strTmp);
			fnHelp();
			printf("\n");
			continue;
		}
		memset(strTmp, '\0', 6 * sizeof(char));

		strncpy(strCommand, strUserCommand, 4);
		strCommand[4] = CHAR_NULL;

		strcpy(strArgument, (strUserCommand + 5));

		for (iCntr = 0; strCommand[iCntr] != CHAR_NULL; ++iCntr) {
			strCommand[iCntr] = tolower(strCommand[iCntr]);
		}

		if (!strcmp("find", strCommand)) {
			/*printf("ARG: %s\n", strArgument);*/

			if (strlen(strArgument) < 1) {
				printf("ERROR: No Argument given!\n");
				fnHelp();
				printf("\n");
				continue;
			}

			if (!fnIsValidName(strArgument)) {
				printf("File Name \"%s\" is not a valid POSIX file name.\n",
						strArgument);
				continue;
			}

			if (access(S_FIFO_FILE, F_OK | W_OK)) {

				printf(
						"Cannot reach server. Make sure about server is working...\n");
				sprintf(strLogMsg,
						"ERROR: Cannot reach server. Make sure about server is working...\n");
				write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
				memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
				continue;
			}

			switch (pidChild = fork()) {
			case -1:
				puts("Failed to fork()\n");
				exit(EXIT_FORK_FAIL);
				break;
			case 0:
				fnFind(argv[1], strArgument);
				exit(EXIT_SUCCESS);
			default:
				/*printf("child pid: %d\n", pidChild);*/
				if (chChilds.iCurrent < CHILD_MAX) {
					chChilds.pidChilds[chChilds.iCurrent] = pidChild;
					strcpy(chChilds.Commands[chChilds.iCurrent], strArgument);
					++(chChilds.iCurrent);

				} else {
					puts("Too many Children. Unable to handle request.\n");
					kill(pidChild, SIGINT);
				}

			}
		} else if (!strcmp("list", strCommand)) {

			if (strlen(strArgument) < 1) {
				printf("ERROR: No Argument given!\n");
				fnHelp();
				printf("\n");
				continue;
			}

			if (!fnIsValidName(strArgument)) {
				printf("File Name \"%s\" is not a valid POSIX file name.\n",
						strArgument);
				continue;
			}

			sprintf(strFileName, C_BACKUP_BASE, getpid(), strArgument);

			if (access(strFileName, F_OK | R_OK)) {
				printf("ERROR: Command \"find %s\" didn't done yet.\n",
						strArgument);
				sprintf(strLogMsg,
						"ERROR: Command \"find %s\" didn't done yet.\n",
						strArgument);
				write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
				memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
				memset(strFileName, '\0', BUFSIZ * sizeof(char));
				continue;
			}
			fptrListFile = fopen(strFileName, "r");
			fnHighlightAndPrint(fptrListFile, strArgument);
			fclose(fptrListFile);
			memset(strFileName, '\0', BUFSIZ * sizeof(char));

		} else if (!strcmp("time", strCommand)) {

			if (strlen(strArgument) < 1) {
				printf("ERROR: No Argument given!\n");
				fnHelp();
				printf("\n");
				continue;
			}

			if (!fnIsValidName(strArgument)) {
				printf("File Name \"%s\" is not a valid POSIX file name.\n",
						strArgument);
				continue;
			}

			sprintf(strFileName, C_BACKUP_BASE_TIME, getpid(), strArgument);

			if (access(strFileName, F_OK | R_OK)) {
				printf("ERROR: Command \"find %s\" didn't done yet.\n",
						strArgument);
				sprintf(strLogMsg,
						"ERROR: Command \"find %s\" didn't done yet.\n",
						strArgument);
				write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
				memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
				memset(strFileName, '\0', BUFSIZ * sizeof(char));
				continue;
			}
			fptrListFile = fopen(strFileName, "r");

			while (!feof(fptrListFile)) {
				fscanf(fptrListFile, "%c", &cTmp);
				printf("%c", cTmp);
				cTmp = '\0';
			}

			fclose(fptrListFile);
			memset(strFileName, '\0', BUFSIZ * sizeof(char));

		} else if (!strcmp("kill", strCommand)) {

			if (strlen(strArgument) < 1) {
				printf("ERROR: No Argument given!\n");
				fnHelp();
				printf("\n");
				continue;
			}

			if (!fnIsValidName(strArgument)) {
				printf("File Name \"%s\" is not a valid POSIX file name.\n",
						strArgument);
				continue;
			}

			for (iCntr = 0; iCntr < chChilds.iCurrent; ++iCntr) {
				if (!strcmp(strArgument, chChilds.Commands[iCntr])) {
					kill(chChilds.pidChilds[iCntr], SIGKILL);
					sprintf(strShared, C_BACKUP_BASE, getpid(), strArgument);
					remove(strShared);
					memset(strShared, '\0', (BUFSIZ * sizeof(char)));
				}
			}

		} else if (!strcmp("quit", strCommand)) {
			fnExitHandler(EXIT_SUCCESS);
			exit(EXIT_SUCCESS);
		} else if (!strcmp("help", strCommand)) {
			fnHelp();
		} else if (!strcmp("clrs", strCommand)) {
			system("clear");
		} else {
			printf("Unrecognized command: %s\n\n", strCommand);
			fnHelp();
			printf("\n");
		}

	}

	return (EXIT_SUCCESS);
}

int fnUsage(const CHAR_PTR cptrExecName) {
	printf("Usage:\n------\n");
	printf("%s [DIR PATH]\n", cptrExecName);
	printf("DIR PATH - Path of directory to list.\n\n");
	printf("Terminal Commands:\n------------------\n");
	printf("find \"[TERM]\" - Searches TERM in directory list.\n");
	printf("list \"[TERM]\" - Lists result of \"find [TERM]\" command.\n");
	printf(
			"time \"[TERM]\" - Prints elapsed of searching TERM in directory list.\n");
	printf("kill \"[TERM]\" - Kill process which searching TERM.\n");
	printf("help - Prints terminal help message.\n");
	printf("clrs - Clears terminal screen.\n");
	printf("quit - Kills all child processes, generates a log and exits.\n");

	return (EXIT_USAGE_PRINT);
}

void fnHelp(void) {
	printf("Terminal Commands:\n------------------\n");
	printf("find \"[TERM]\" - Searches TERM in directory list.\n");
	printf("list \"[TERM]\" - Lists result of \"find [TERM]\" command.\n");
	printf(
			"time \"[TERM]\" - Prints elapsed of searching TERM in directory list.\n");
	printf("kill \"[TERM]\" - Kill process which searching TERM.\n");
	printf("help - Prints this help message.\n");
	printf("clrs - Clears terminal screen.\n");
	printf("quit - Kills all child processes, generates a log and exits.\n");
}

static void fnSigIntHandler(int iSigNum) {

	char strShared[BUFSIZ];
	memset(strShared, '\0', BUFSIZ * sizeof(char));

	if (pidOrigin != getpid()) {
		kill(getppid(), SIGINT);
		return;
	}

	tcsetattr(STDIN_FILENO, TCSANOW, &tOrigin);

	printf("INFO: SIGINT caught, exiting properly...\n");
	fflush(stdout);

	sprintf(strShared, "INFO: SIGINT caught, exiting properly...\n");
	write(fdLogFile, strShared, (strlen(strShared) * sizeof(char)));
	memset(strShared, '\0', BUFSIZ * sizeof(char));

	fnExitHandler(EXIT_SIGINT);

}

static void fnSigUsr1Handler(int iSigNum) {

	char strShared[BUFSIZ];
	memset(strShared, '\0', BUFSIZ * sizeof(char));

	if (pidOrigin != getpid()) {
		kill(getppid(), SIGUSR1);
		return;
	}

	tcsetattr(STDIN_FILENO, TCSANOW, &tOrigin);

	printf("INFO: Shutdown Signal from server caught, exiting properly...\n");
	fflush(stdout);

	sprintf(strShared,
			"INFO: Shutdown Signal from server caught, exiting properly...\n");
	write(fdLogFile, strShared, (strlen(strShared) * sizeof(char)));
	memset(strShared, '\0', BUFSIZ * sizeof(char));

	fnExitHandler(EXIT_SIGUSR1);
}

static void fnExitHandler(int iExitCode) {

	TIMEVAL tEnd;
	long lliElapsedTime = 0;
	int iCntr = 0;
	char strShared[BUFSIZ];
	char strSharedTime[BUFSIZ];
	char strLogMsg[BUFSIZ];

	time_t tRawFormat;

	FILEDES fdServer;

	memset(strShared, '\0', BUFSIZ * sizeof(char));
	memset(strSharedTime, '\0', BUFSIZ * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	if (gettimeofday(&tEnd, NULL )) {
		sprintf(strLogMsg, "ERROR: Failed to get ending time.\n");
		write(fdLogFile, strLogMsg, (strlen(strLogMsg) * sizeof(char)));
		memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
	}

	while (0 < fnWaitChilds(NULL ))
		;

	for (iCntr = 0; iCntr < chChilds.iCurrent; ++iCntr) {
		sprintf(strShared, C_BACKUP_BASE, getpid(), chChilds.Commands[iCntr]);
		sprintf(strSharedTime, C_BACKUP_BASE_TIME, getpid(),
				chChilds.Commands[iCntr]);
		remove(strShared);
		remove(strSharedTime);
		memset(strShared, '\0', BUFSIZ * sizeof(char));
	}

	close(fdServerClient);
	unlink(strServerClientFifo);

	sprintf(strLogMsg, "INFO: Restoring original terminal configuration.\n");
	write(fdLogFile, strLogMsg, (strlen(strLogMsg) * sizeof(char)));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	tcsetattr(STDIN_FILENO, TCSANOW, &tOrigin);

	lliElapsedTime = fnTimeDiff(tBegin, tEnd);
	sprintf(strLogMsg, "INFO: Elapsed Time: %ld msec(s)\n", lliElapsedTime);
	write(fdLogFile, strLogMsg, (strlen(strLogMsg) * sizeof(char)));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	time(&tRawFormat);
	sprintf(strLogMsg, "INFO: Ending Time: %s", ctime(&tRawFormat));
	write(fdLogFile, strLogMsg, (strlen(strLogMsg) * sizeof(char)));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	sprintf(strLogMsg, "----   END OF LOG FILE   ----\n");
	write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	close(fdLogFile);

	fdServer = open(S_FIFO_FILE, O_WRONLY);
	sprintf(strLogMsg, "C %d C 1", getpid());
	write(fdServer, strLogMsg, strlen(strLogMsg) * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
	close(fdServer);

	sprintf(strLogMsg, "mv -f %s .", strLogFile);
	/*sprintf(strLogMsg, "cp -f %s .", strLogFile);*/
	system(strLogMsg);
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	exit(iExitCode);
}

long fnTimeDiff(TIMEVAL tBegin, TIMEVAL tEnd) {
	return ((MILLION * (tEnd.tv_sec - tBegin.tv_sec))
			+ (tEnd.tv_usec - tBegin.tv_usec));
}

pid_t fnWaitChilds(INT_PTR iptrStatLoc) {
	int iRetval;

	while (((iRetval = wait(iptrStatLoc)) == -1) && (errno == EINTR))
		;
	return iRetval;
}

int fnFind(const CHAR_PTR cptrPath, const CHAR_PTR cptrArg) {

	char strClientServerFifo[PATH_MAX];

	char strShared[BUFSIZ];
	char strLogMsg[BUFSIZ];
	char strFileName[BUFSIZ];
	char strToReplace[PATH_MAX];

	char cTmp = '\0';

	TIMEVAL tStart, tDone;
	long lliElapsedTime = 0L;

	FILE_PTR fptrBackup = NULL;
	FILE_PTR fptrTime = NULL;

	memset(strClientServerFifo, '\0', PATH_MAX * sizeof(char));
	memset(strShared, '\0', BUFSIZ * sizeof(char));
	memset(strToReplace, '\0', BUFSIZ * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
	memset(strFileName, '\0', BUFSIZ * sizeof(char));

	if (gettimeofday(&tStart, NULL )) {
		sprintf(strLogMsg,
				"ERROR: Failed to get ending time in function fnFind().\n");
		write(fdLogFile, strLogMsg, (strlen(strLogMsg) * sizeof(char)));
		memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
	}

	sprintf(strShared, "D %d %s %d", getpid(), cptrPath, getppid());

	if ((fdClientServer = open(S_FIFO_FILE, O_WRONLY)) == -1) {
		printf("Cannot reach server. Make sure about server is working...\n");
		sprintf(strLogMsg,
				"ERROR: Cannot reach server. Make sure about server is working...\n");
		write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
		memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
		exit(EXIT_FIFO_CREATE_FAIL);
	}

	/*write(fdClientServer, strShared, (strlen(strShared) * sizeof(char)));*/
	write(fdClientServer, strShared, (BUFSIZ * sizeof(char)));
	close(fdClientServer);

	sprintf(strLogMsg, "INFO: Sending request to Server...\n");
	write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	memset(strShared, 0, sizeof(strShared));

	sprintf(strServerClientFifo, SC_FIFO_BASE, getpid(), 0);
	mkfifo(strServerClientFifo, S_FIFO_MODE);

	fdServerClient = open(strServerClientFifo, O_RDONLY);

	sprintf(strShared, C_BACKUP_BASE, getppid(), cptrArg);

	fptrBackup = fopen(strShared, "w");
	memset(strShared, 0, sizeof(strShared));

	while (read(fdServerClient, &cTmp, sizeof(char))) {
		fprintf(fptrBackup, "%c", cTmp);
	}
	close(fdServerClient);
	fflush(fptrBackup);
	fclose(fptrBackup);

	unlink(strServerClientFifo);

	memset(strShared, 0, sizeof(strShared));

	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	if (gettimeofday(&tDone, NULL )) {
		sprintf(strLogMsg,
				"ERROR: Failed to get ending time in function fnFind().\n");
		write(fdLogFile, strLogMsg, (strlen(strLogMsg) * sizeof(char)));
		memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
	}

	lliElapsedTime = fnTimeDiff(tStart, tDone);

	sprintf(strFileName, C_BACKUP_BASE_TIME, getppid(), cptrArg);

	fptrTime = fopen(strFileName, "w");

	fprintf(fptrTime, "%ld msec(s) elapsed when searching for term \"%s\".\n",
			lliElapsedTime, cptrArg);
	fclose(fptrTime);
	memset(strFileName, '\0', BUFSIZ * sizeof(char));

	sprintf(strLogMsg, "INFO: Client %d: Done Find. Elapsed Time %ld msec(s)\n",
			getpid(), lliElapsedTime);
	write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	exit(EXIT_SUCCESS);

	return 1;
}

char fnReadChar(int iFileDes) {
	char cReadChar = 0;
	TERMIOS_T tActive, tBackup;

	tcgetattr(iFileDes, &tBackup);
	tcgetattr(iFileDes, &tActive);

	/* Make active configuration of terminal non-canonical */
	tActive.c_lflag &= ~(ICANON | ECHO | ECHOE | ECHOK | ECHONL | ICRNL);

	tcsetattr(iFileDes, TCSANOW, &tActive);

	read(iFileDes, &cReadChar, sizeof(char));

	tcsetattr(iFileDes, TCSANOW, &tBackup);

	return cReadChar;
}

UNSIGINT fnPrintLines(const CHAR_PTR cptrBuff, const UNSIGINT uBuffLen,
		const UNSIGINT uLineCount, const UNSIGINT uLineWidth) {
	UNSIGINT uLinesPrint = 0;
	UNSIGINT uIndex = 0;
	UNSIGINT uCharsPrint = 0;
	char cToPrint = 0;

	while (uIndex < uBuffLen) {
		cToPrint = cptrBuff[uIndex++];
		++uCharsPrint;
		putchar(cToPrint);
		if (CHAR_NEW_LINE == cToPrint) {
			uCharsPrint = 0;
			++uLinesPrint;
		} else if (0 == (uCharsPrint % (uLineWidth))) {
			uCharsPrint = 0;
			++uLinesPrint;
			putchar(CHAR_NEW_LINE); /* Finish current line */
		}

		if (uLinesPrint >= uLineCount)
			break;
	}

	return uIndex;
}

int fnGetStreamWidth(int iFileDes) {
	WINSIZE_T wWinsize;
	ioctl(iFileDes, TIOCGWINSZ, &wWinsize);

	return (wWinsize.ws_col);
}

UNSIGINT fnListFile(FILE_PTR fptrFile) {
	UNSIGINT uPrintChars = 0;
	UNSIGINT uLineWidth = DEAFULT_LINE_WIDTH;
	UNSIGINT uLinePerScrn = DEFAULT_LINE_PER_SCREEN;
	UNSIGINT uCharCount = 0;

	char cUserCommand = 0;
	UNSIGINT uPos = 0;

	CHAR_PTR cptrFileBuff = NULL;

	uLineWidth = fnGetStreamWidth(STDOUT_FILENO);

	uCharCount = fnCharCount(fptrFile);

	if (!(cptrFileBuff = (CHAR_PTR) malloc(sizeof(char) * uCharCount))) {
		printf("Failed to Allocate Memory for Buffering File: %s\n",
				strerror(errno));
		return (EXIT_FAILURE);
	}

	fnCopyFileToBuffer(fptrFile, cptrFileBuff, uCharCount);

	/* TODO MARK */
	uPos = fnPrintLines(cptrFileBuff, uCharCount, uLinePerScrn, uLineWidth);

	while (1) {
		/* Check for EOF */
		if (uPos >= uCharCount)
			break;

		/* Get user's command */
		cUserCommand = fnReadChar(STDIN_FILENO);

		/* Process user's command */
		if (cUserCommand == CHAR_NEW_LINE) {
			uPos += fnPrintLines((cptrFileBuff + uPos), (uCharCount - uPos), 1,
					uLineWidth);
		} else if (cUserCommand == CHAR_SPACE) {
			uPos += fnPrintLines((cptrFileBuff + uPos), (uCharCount - uPos),
					uLinePerScrn, uLineWidth);
		}
	} /* End of printing part */

	return uPrintChars;
}

UNSIGINT fnCharCount(FILE_PTR fptrFile) {
	char chrBuf = 0;
	UNSIGINT uTotalNumOfChars = 0;

	while (!feof(fptrFile)) {
		fscanf(fptrFile, "%c", &chrBuf);
		++uTotalNumOfChars;
	}

	/* Return to beginning of file */
	rewind(fptrFile);

	return uTotalNumOfChars;
}

int fnCopyFileToBuffer(FILE_PTR fptrFile, CHAR_PTR cptrBuff,
		UNSIGINT uNumOfChars) {
	UNSIGINT iCntr = 0;

	for (iCntr = 0; iCntr < (uNumOfChars - 1); ++iCntr) {
		cptrBuff[iCntr] = fgetc(fptrFile);
		if (EOF == cptrBuff[iCntr])
			break;
	}

	/* Make Sure String is terminated*/
	cptrBuff[iCntr] = CHAR_NULL;

	return iCntr;
}

int fnIsValidName(const CHAR_PTR cptrName) {

	size_t sCount = 0;
	char cCurrent = '\0';
	const size_t sLen = strlen(cptrName);

	if (0 == sLen) {
		return 0;
	}

	for (sCount = 0; sCount < sLen; ++sCount) {
		cCurrent = cptrName[sCount];
		if (!((cCurrent >= 'A' && cCurrent <= 'Z')
				|| (cCurrent >= 'a' && cCurrent <= 'z')
				|| (cCurrent >= '0' && cCurrent <= '9') || cCurrent == '~'
				|| cCurrent == '.' || cCurrent == '-')) {

			return 0;
		}
	}

	return 1;
}

void fnInsert(CHAR_PTR cptrString, int iIndex, const char cToInsert) {
	int iCtr;
	cptrString[strlen(cptrString) + 1] = '\0';

	for (iCtr = strlen(cptrString); iCtr > iIndex; --iCtr) {
		cptrString[iCtr] = cptrString[iCtr - 1];
	}
	cptrString[iIndex] = cToInsert;
}

int fnHighlight(CHAR_PTR cptrString, CHAR_PTR cptrKey) {
	int iCtr1 = 0;
	int iCtr2 = 0;
	int iKeyIsMatching = 0;
	int iCount = 0;

	for (iCtr1 = 0; iCtr1 < strlen(cptrString); ++iCtr1) {
		if (cptrString[iCtr1] == cptrKey[0]) {
			iKeyIsMatching = 1;
			++iCtr1;
			for (iCtr2 = 1; iCtr2 < strlen(cptrKey) && iKeyIsMatching;
					++iCtr1, ++iCtr2) {
				if (cptrString[iCtr1] != cptrKey[iCtr2])
					iKeyIsMatching = 0;
			}

			/* If key is found, highlight it with " " */
			if (iKeyIsMatching) {
				fnInsert(cptrString, iCtr1 - strlen(cptrKey), '\"');
				fnInsert(cptrString, iCtr1 + 1, '\"');
				iCtr1 += 2;
				++iCount;
			}
		}
	}

	return (iCount);
}
void fnHighlightAndPrint(FILE_PTR fptrFile, const CHAR_PTR cptrArg) {
	CHAR_PTR_PTR cptrptrFileBuf = NULL;
	int iCntr = 0;
	int iLine = 0;
	int iCount = 0;
	char cTmp = '\0';
	FILE_PTR fptrBuf = NULL;
	char strShared[S_LOG_NAME_MAX];

	memset(strShared, '\0', sizeof(char) * S_LOG_NAME_MAX);

	cptrptrFileBuf = (CHAR_PTR_PTR) malloc(sizeof(CHAR_PTR) * (LINE_LEN));
	for (iCntr = 0; iCntr < LINE_LEN; ++iCntr) {
		cptrptrFileBuf[iCntr] = (CHAR_PTR) malloc(sizeof(char) * (LINE_LEN));
	}

	for (iCntr = 0; iCntr < LINE_LEN; ++iCntr) {
		memset(cptrptrFileBuf[iCntr], '\0', (sizeof(char) * LINE_LEN));
	}

	iCntr = 0;
	iLine = 0;

	while (!feof(fptrFile)) {
		fscanf(fptrFile, "%c", &cTmp);
		cptrptrFileBuf[iLine][iCntr] = cTmp;
		++iCntr;
		if (cTmp == '\n') {
			cTmp = '\0';
			++iLine;
			iCntr = 0;
		}
	}

	sprintf(strShared, C_BACKUP, getpid(), cptrArg);

	fptrBuf = fopen(strShared, "w");

	for (iCount = 0; iCount < iLine; ++iCount) {
		if (strstr(cptrptrFileBuf[iCount], cptrArg)) {
			fnHighlight(cptrptrFileBuf[iCount], cptrArg);
			fprintf(fptrBuf, "%s", cptrptrFileBuf[iCount]);
		}
	}
	fflush(fptrBuf);
	fclose(fptrBuf);

	fptrBuf = fopen(strShared, "r");
	fnListFile(fptrBuf);
	fclose(fptrBuf);
	remove(strShared);

	for (iCntr = 0; iCntr < LINE_LEN; ++iCntr) {
		free(cptrptrFileBuf[iCntr]);
	}
	free(cptrptrFileBuf);

}
