/*
 * tellmeMORE.h
 *
 *  Created on: Apr 21, 2013
 *      Author: Mehmet Akif TAŞOVA
 *      Student Number: 111044016
 *
 */

#ifndef TELLMEMORE_H_
#define TELLMEMORE_H_

#include <signal.h>
#include <sys/time.h>
#include <bits/sigaction.h>
#include <termios.h>
#include <sys/ioctl.h>

#define NAME_MAX         255	/* # chars in a file name */
#define PATH_MAX        4096	/* # chars in a path name including nul */
#define PIPE_BUF        4096	/* # bytes in atomic write to a pipe */

#define LINE_LEN 512

#define CMD_MAX 5

#define NOT_FOUND -1

#define EXIT_USAGE_PRINT 1

/* Exit after failed to get current tme of day */
#define EXIT_TIME_GET_FAIL 2

/* Exit code for when exiting after SIGINT */
#define EXIT_SIGINT 3

/* Exit code which used when failed to creade server-client fifo file */
#define EXIT_FIFO_CREATE_FAIL 4

/* Exit code which used when failed to set signal handler */
#define EXIT_SIGHANDLER_FAIL 5

#define EXIT_FORK_FAIL 5

#define EXIT_SIGUSR1 6

#define S_FIFO_FILE "/tmp/lsserver"
#define S_FIFO_MODE 0666

#define SC_FIFO_BASE "/tmp/client-%d-%d"

#define C_BACKUP "/tmp/%d-%s-done"
#define C_BACKUP_BASE "/tmp/%d-%s" /*means "/tmp/ppid-term" */
#define C_BACKUP_BASE_TIME "/tmp/%d-%s-time"

#define S_TMP_LOG "/tmp/tellmeMORElog-"
#define S_LOG_NAME_MAX 30

#define MILLION 1000000L

#define CHILD_MAX 100

#define DEFAULT_LINE_PER_SCREEN 24
#define DEAFULT_LINE_WIDTH 80

/* Some useful char values */
#define CHAR_NEW_LINE '\n'
#define CHAR_SPACE ' '
#define CHAR_NULL '\0'

typedef struct {
    	char strPathToList[BUFSIZ];
	char strToSeach[NAME_MAX];
} ARGUMENT_T;

typedef ARGUMENT_T* ARGUMENT_PTR;

/* typedef for standard C identifiers */
typedef void* VOID_PTR;

typedef char * CHAR_PTR;

typedef char ** CHAR_PTR_PTR;

typedef int * INT_PTR;

typedef FILE * FILE_PTR;

typedef unsigned int UNSIGINT;

/* File Descriptor type */
typedef int FILEDES;

/* Type of timeval */
typedef struct timeval TIMEVAL;

typedef struct sigaction SIGACTION;

/* termios.h related typedefs*/
typedef struct termios TERMIOS_T;

/* ioctl.h related typedefs*/
typedef struct winsize WINSIZE_T;

typedef struct {
	pid_t pidChilds[CHILD_MAX];
	char Commands[CHILD_MAX][BUFSIZ];
	int iCurrent;
} CHILD_T;

/*
 * Prints usage and returns code as acceptable as exit code
 */
int fnUsage(const CHAR_PTR cptrExecName);

/*
 * Prints commands of internal terminal
 */
void fnHelp(void);

/* Signal handler for SIGINT */
static void fnSigIntHandler(int iSigNum);

/* Signal handler for SIGUSR1 */
static void fnSigUsr1Handler(int iSigNum);

/* Exit handler function */
static void fnExitHandler(int iExitCode);

/* Returns difference between tBegin and tEnd */
long fnTimeDiff(TIMEVAL tBegin, TIMEVAL tEnd);

/* Function for waiting all child proccess to terminate */
pid_t fnWaitChilds(INT_PTR iptrStatLoc);

/* The function which will be called when user gives find command */
VOID_PTR fnFind(VOID_PTR args);

/* Reads a single char from given File Descriptor, then returns it */
char fnReadChar(int iFileDes);

/*
 * Takes text in cptrBuff and prints uLineCount line of it where one line is
 *   uLineWidth characters width. Returns number of printed characters
 */
UNSIGINT fnPrintLines(const CHAR_PTR cptrBuff, const UNSIGINT uBuffLen,
		const UNSIGINT uLineCount, const UNSIGINT uLineWidth);

/* Returns given file streams width value */
int fnGetStreamWidth(int iFileDes);

/* Lists given file's ingredients to stdout */
UNSIGINT fnListFile(FILE_PTR fptrFile);

/*
 * Counts all characters in given file by reading them one by one.
 * When counting done, rewinds file to beginning
 */
UNSIGINT fnCharCount(FILE_PTR fptrFile);

/*
 * Reads every single character in given file to given buffer.
 * Memory for buffer must be allocated before sending
 *	pointer of buffer to function.
 *
 * Returns total number of read characters from file
 */
int fnCopyFileToBuffer(FILE_PTR fptrFile, CHAR_PTR cptrBuff,
		UNSIGINT uNumOfChars);

/*
 * Checks is given file name is a valid POSIX file name. returns 1 if true
 *  otherwise returns 0
 */
int fnIsValidName(const CHAR_PTR cptrName);

/* Inserts character to string at given index. */
void fnInsert(CHAR_PTR cptrString, int iIndex, const char cToInsert);

/* Highlights ("") the key in string. */
int fnHighlight(CHAR_PTR cptrString, CHAR_PTR cptrKey);

/* Highlights and Prints ingredients of a file */
void fnHighlightAndPrint(FILE_PTR fptrFile, const CHAR_PTR cptrArg);

#endif /* TELLMEMORE_H_ */
