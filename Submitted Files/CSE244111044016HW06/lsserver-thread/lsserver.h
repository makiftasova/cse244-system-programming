/*
 * lsserver.h
 *
 *  Created on: Apr 21, 2013
 *      Author: Mehmet Akif TAŞOVA
 *      Student Number: 111044016
 *
 */

#ifndef LSSERVER_H_
#define LSSERVER_H_

#include <string.h>
#include <signal.h>
#include <sys/time.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>

#define NAME_MAX         255	/* # chars in a file name */
#define PATH_MAX        4096	/* # chars in a path name including nul */
#define PIPE_BUF        4096	/* # bytes in atomic write to a pipe */

/*
 * Maximum number of clients can connect at same time
 */
#define MAX_CLIENTS 100

#define USAGE_MSG_SIZ 90
#define LD_LINE_LEN 512

/* Exit after usage print */
#define EXIT_USAGE_PRINT 1

/* Exit after failed to get current tme of day */
#define EXIT_TIME_GET_FAIL 2

/* Exit code for when exiting after SIGINT */
#define EXIT_SIGINT 3

/* Exit code which used when failed to creade server-client fifo file */
#define EXIT_FIFO_CREATE_FAIL 4

/* Exit code which used when failed to set signal handler */
#define EXIT_SIGHANDLER_FAIL 5

#define EXIT_FORK_FAIL 6

/* Exit code for failing to open directory */
#define EXIT_PERM_DENIED 20

/* Exit code for failed to determine maximum length of path */
#define EXIT_FAIL_DETERMINE_PATH_LEN 21

/* Exit code for failed to allocate memory for storing pathname */
#define EXIT_PATHNAME_ALLOC_FAIL 22

/* Exit code for failed to get current working directory */
#define EXIT_GETCWD_FAIL 23

#define S_FIFO_FILE "/tmp/lsserver"
#define S_FIFO_MODE 0666

#define SC_FIFO_BASE "/tmp/%s-list"

#define S_TMP_LOG "/tmp/lsserverlog-"
#define S_LOG_NAME_MAX 30

#define MILLION 1000000L

#define MAX_CHILD 100
#define PID_MAX 10

/* A little string which always points cwd when used as dir path */
#define CURRENT_DIR "."

/*
 * A little string which always points parent dir of cwd
 *  when used as dir path
 */
#define PARENT_DIR ".."

/* Some useful defines for using with system function */
#define FAIL -1
#define SUCCESS 0

/* Character pointer type */
typedef void* VOID_PTR;

typedef char * CHAR_PTR;

typedef int * INT_PTR;

/* File Descriptor type */
typedef int FILEDES;


/*
 * A struct for passing arguments of fnListDir() to another thread since
 *  ptherad_create() only accepts one argument for arguments of start_routine
 */
typedef struct {
	char strPathToList[BUFSIZ];
	FILEDES fdFileDes;
} ARGUMENT_T;

typedef ARGUMENT_T* ARGUMENT_PTR;

/* Type of timeval */
typedef struct timeval TIMEVAL;

/* typedefs for types from dirent.h */
typedef DIR* DIR_PTR;

typedef struct dirent DIRENT;

typedef DIRENT* DIRENT_PTR;

typedef DIRENT** DIRENT_PTRPTR;

/* typedefs for types from sys/stat.h */
typedef struct stat STAT;

/* A structure for client list. */
typedef struct {
	pid_t pidClients[MAX_CLIENTS];
	int iCurrrent;
	int iSize;
} CLIENTLIST_T;

/* Prints usage message to STDOUT */
int fnUsage(const CHAR_PTR cptrExecName);

/* Returns difference between tBegin and tEnd */
long fnTimeDiff(TIMEVAL tBegin, TIMEVAL tEnd);

/* Directory list function */
int fnListDir(const CHAR_PTR cptrPathToList, FILEDES fdFIFO);

/* Prints error message with perror. */
void fnPrintError(const CHAR_PTR cptrMessage);

/* Takes a file path, then checks is given file is a directory or not. */
int fnIsDirectory(const CHAR_PTR ccptrPath);

/* Workload of child proccess which lists the directory */
VOID_PTR fnDirListChild(VOID_PTR args);

/* Function for waiting all child proccess to terminate */
pid_t fnWaitChilds(INT_PTR iptrStatLoc);

/*
 * Checks child pid, if they are not alive any more removes them from child list
 */
void fnCheckChilds(void);

/*
 * Checks given lient pid in clCleints list. If client pid exists returns 1,
 * otherwise 0.
 */
int fnIsKnownClient(const pid_t pidClient);

#endif /* LSSERVER_H_ */
