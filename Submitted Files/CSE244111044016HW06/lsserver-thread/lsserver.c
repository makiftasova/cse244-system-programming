/*
 * lsserver.c
 *
 *  Created on: Apr 21, 2013
 *      Author: Mehmet Akif TAŞOVA
 *      Student Number: 111044016
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <sys/signal.h>
#include <pthread.h>

#include "lsserver.h"

/*
 * Some unavoidable globals
 */
CLIENTLIST_T clClients;

TIMEVAL tBegin;
FILEDES fdServerClientFifo;
FILEDES fdLogFile;
char strLogFile[S_LOG_NAME_MAX];
pid_t pidParent;

pid_t pidChildList[MAX_CHILD];
int iNumOfChilds;

int main(int argc, CHAR_PTR argv[])
{

	char cClientCommand;
	char strLogMsg[BUFSIZ];

	/* The directory path where program begins */
	char strRawClientData[BUFSIZ];
	char strClientPath[PATH_MAX];
	char strClientPid[PID_MAX];

	time_t tRawFormat;

	FILEDES fdClientServer;
	char strPath[PATH_MAX];
	char strMsg[BUFSIZ];
	ARGUMENT_T args;
	pthread_t pthID;
	pthread_attr_t pthAttr;

	pthread_attr_init(&pthAttr);
	pthread_attr_setdetachstate(&pthAttr, PTHREAD_CREATE_DETACHED);

	pidParent = getpid();

	memset(strPath, '\0', PATH_MAX * sizeof(char));
	memset(strMsg, '\0', BUFSIZ * sizeof(char));
	memset(args.strPathToList, '\0', BUFSIZ * sizeof(char));

	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
	memset(strRawClientData, '\0', BUFSIZ * sizeof(char));
	memset(strClientPath, '\0', PATH_MAX * sizeof(char));
	memset(strClientPid, '\0', PID_MAX * sizeof(char));

	iNumOfChilds = 0;

	clClients.iSize = MAX_CLIENTS;
	clClients.iCurrrent = 0;

	pidParent = getpid();

	if (argc == 2) {
		if (!strcmp(argv[1], "-h")) {
			return(fnUsage(argv[0]));
		} else if (!strcmp(argv[1], "--help")) {
			return(fnUsage(argv[0]));
		} else {
			printf("Unrecognized argument: %s\n", argv[1]);
			return(fnUsage(argv[0]));
		}
	} else if (argc != 1) {
		return(fnUsage(argv[0]));
	}

	sprintf(strLogFile, "%s%d%s", S_TMP_LOG, pidParent, ".log");
	fdLogFile = open(strLogFile, O_WRONLY | O_APPEND | O_CREAT,
		S_IRUSR | S_IWUSR);

	sprintf(strLogMsg, "INFO: Starting lsserver...\n");
	write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	time(&tRawFormat);
	sprintf(strLogMsg, "INFO: Starting Time: %s", ctime(&tRawFormat));
	write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	sprintf(strLogMsg, "INFO: Creating FIFO: %s\n", S_FIFO_FILE);
	write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	mkfifo(S_FIFO_FILE, S_FIFO_MODE);

	sprintf(strLogMsg, "INFO: FIFO: %s is ready.\n", S_FIFO_FILE);
	write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	fdServerClientFifo = open(S_FIFO_FILE, O_RDONLY | O_APPEND | O_CREAT,
		S_IRUSR | S_IWUSR);

	while (1) {

		if (read(fdServerClientFifo, strRawClientData, BUFSIZ * sizeof(char))
			< 0) {
			sprintf(strLogMsg, "ERROR: Error reading FIFO: %s\n",
				strerror(errno));
			write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
			memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
		}
		printf("read\n");

		sscanf(strRawClientData, "%c %s %s", &cClientCommand, strClientPid,
			args.strPathToList);

		memset(strPath, '\0', PATH_MAX * sizeof(char));
		sprintf(strPath, "/tmp/%s-list", strClientPid);
		fdClientServer = open(strPath, O_WRONLY | O_APPEND | O_CREAT,
			S_IRUSR | S_IWUSR);
		memset(strPath, '\0', PATH_MAX * sizeof(char));

		args.fdFileDes = fdClientServer;

		pthread_create(&pthID, &pthAttr, fnDirListChild, (VOID_PTR) & args);

		memset(strRawClientData, '\0', BUFSIZ * sizeof(char));
		memset(strClientPid, '\0', PID_MAX * sizeof(char));
		memset(strClientPath, '\0', PATH_MAX * sizeof(char));
		memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
	}

	return(EXIT_SUCCESS);
}

int fnUsage(const CHAR_PTR cptrExecName)
{

	printf("Usage:\n------\n");
	printf(
		"%s [OPTION]- A Daemon for listing given directory and its sub directories.\n",
		cptrExecName);
	printf("\nOPTION could be:\n");
	printf("-h or --help - Prints this help message.\n");

	return(EXIT_USAGE_PRINT);
}

long fnTimeDiff(TIMEVAL tBegin, TIMEVAL tEnd)
{
	return((MILLION * (tEnd.tv_sec - tBegin.tv_sec))
		+ (tEnd.tv_usec - tBegin.tv_usec));
}

int fnListDir(const CHAR_PTR cptrPathToList, FILEDES fdFIFO)
{

	DIRENT_PTR deptrDirent = NULL;
	DIR_PTR dptrDir = NULL; /* pointer to current dir */
	char cptrNewPath[BUFSIZ];

	int iNumOfSubDirs = 0; /* number of sub-directories of cwd */

	char strPrintLine[LD_LINE_LEN];
	int iPrintLine = 0;
	char strMsg[BUFSIZ];

	memset(strPrintLine, '\0', LD_LINE_LEN * sizeof(char));
	memset(strMsg, '\0', BUFSIZ * sizeof(char));
	memset(cptrNewPath, '\0', BUFSIZ * sizeof(char));

	if (!(dptrDir = opendir(cptrPathToList))) {
		sprintf(strMsg, "WARN: Child %d: Failed to open directory: %s : %s\n",
			getpid(), cptrPathToList, strerror(errno));
		fnPrintError(strMsg);
		memset(strMsg, '\0', BUFSIZ * sizeof(char));
		return(iNumOfSubDirs);
	}

	while ((deptrDirent = readdir(dptrDir))) {
		if ((strcmp(deptrDirent->d_name, CURRENT_DIR))
			&& (strcmp(deptrDirent->d_name, PARENT_DIR))) {

			if (cptrPathToList[strlen(cptrPathToList) - 1] != '/') {
				strcat(cptrPathToList, "/");
			}

			/* Write found files to FIFO */
			sprintf(strPrintLine, "%s%s\n", cptrPathToList,
				deptrDirent->d_name);
			iPrintLine = (strlen(strPrintLine) * sizeof(char));
			write(fdFIFO, strPrintLine, iPrintLine);

			memset(cptrNewPath, '\0', BUFSIZ * sizeof(char));

			sprintf(cptrNewPath, "%s%s", cptrPathToList, deptrDirent->d_name);

			if (fnIsDirectory(cptrNewPath)) {
				++iNumOfSubDirs;

				if ((FAIL == (chdir(cptrNewPath))) && (errno == EACCES)) {

					sprintf(strMsg,
						"WARN: Child %d: Cannot open directory: %s: %s\n",
						getpid(), cptrNewPath, strerror(errno));
					fnPrintError(strMsg);
					memset(strMsg, '\0', BUFSIZ * sizeof(char));

				} else {

					chdir(cptrNewPath);
					iNumOfSubDirs += fnListDir(cptrNewPath, fdFIFO);
				}
				/*free(cptrNewPath);*/
				chdir(cptrPathToList);

			} else {
				continue;
			}

		}
	}

	while ((closedir(dptrDir) == FAIL) && (errno == EINTR))
		;

	return(iNumOfSubDirs);
}

void fnPrintError(const CHAR_PTR cptrMessage)
{

	write(fdLogFile, cptrMessage, strlen(cptrMessage) * sizeof(char));

	return;

}

int fnIsDirectory(const CHAR_PTR ccptrPath)
{
	STAT stStatBuffer;

	if (FAIL == stat(ccptrPath, &stStatBuffer)) {

		return 0;
	}

	return S_ISDIR(stStatBuffer.st_mode);
}

VOID_PTR fnDirListChild(VOID_PTR args)
{

	ARGUMENT_T arg = *((ARGUMENT_PTR) args);

	fnListDir(arg.strPathToList, arg.fdFileDes);

	/*close(arg.fdFileDes);*/

	return args;
}

pid_t fnWaitChilds(INT_PTR iptrStatLoc)
{
	int iRetval;

	while (((iRetval = wait(iptrStatLoc)) == -1) && (errno == EINTR))
		;
	return iRetval;
}

void fnCheckChilds(void)
{
	int iCntr = 0;
	int iStat;

	for (iCntr = 0; iCntr < iNumOfChilds; ++iCntr) {
		waitpid(pidChildList[iCntr], &iStat, WNOHANG);
		if (WIFEXITED(iStat)) {
			if (MAX_CLIENTS > iCntr) {
				if (MAX_CLIENTS == (iCntr + 1)) {
					pidChildList[iCntr] = 0;
				} else {
					pidChildList[iCntr] = pidChildList[iCntr + 1];
				}
			}
		}
	}
}

int fnIsKnownClient(const pid_t pidClient)
{
	int iCurr = clClients.iCurrrent;
	int iCntr = 0;

	for (iCntr = 0; iCntr < iCurr; ++iCntr) {
		if (pidClient == clClients.pidClients[iCntr]) {
			return 1;
		}
	}
	return 0;
}
/* End of lsserver.c */
