/* 
 * File:   lsnofork.c
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * CSE244 HW02 Part01
 * 
 * A little modified version of well known ls
 * 
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>




#include "lsnofork.h"

/*
 * 
 */
int main(int argc, CHAR_PTR argv[])
{
	/* The directory path where program begins */
	CHAR_PTR cptrBeginDir = NULL;

	CHAR_PTR cptrCurrDir = NULL; /* cwd for listing ingredients */


	/* maximum  possible name length of a pathname */
	long long int lMaxPathNameLen = 0;

	int iCntr = 0; /* generic counter */
	int iTmpCntr = 0; /* a counter for some unnecessary things */

	if ((argc < 1) || (argc > 2))
		return fnUsage(argv[0]);

	cptrBeginDir = ((1 == argc) ? CURRENT_DIR : argv[1]);

	if (FAIL == (lMaxPathNameLen = pathconf(CURRENT_DIR, _PC_PATH_MAX))) {
		perror("Failed to determine maximum length of path\n");
		return(EXIT_FAIL_DETERMINE_PATH_LEN);
	}

	if (!(cptrCurrDir = (CHAR_PTR) malloc(lMaxPathNameLen))) {
		perror("Failed to allocate memory for storing pathname\n");
		return(EXIT_PATHNAME_ALLOC_FAIL);
	}

	if ((FAIL == (chdir(cptrBeginDir))) && (errno == EACCES)) {
		fnPrintPermDeniedErr(argv[0], cptrBeginDir);
		return(EXIT_PERM_DENIED);

	}

	if (!(getcwd(cptrCurrDir, lMaxPathNameLen))) {
		perror("Failed to get current working directory\n");
		return(EXIT_GETCWD_FAIL);
	}

	iCntr = fnPrintBeginPrint(cptrCurrDir);
	printf(" PID%25cPATH\n", ' ');
	printf("-----%5c", ' ');

	for (iTmpCntr = 0; iTmpCntr < (iCntr - 10); ++iTmpCntr)
		putchar('-');
	putchar('\n');

	fnListDir(cptrCurrDir, argv[0]);

	fnEndPrint(iCntr);

	return(EXIT_SUCCESS);
}

int fnUsage(const CHAR_PTR cptrExecName)
{
	printf("usage:\n------\n%s [DIRPATH]\n", cptrExecName);

	return(EXIT_USAGE_PRINT);
}

BOOL_T fnIsDirectory(const CHAR_PTR ccptrPath)
{
	STAT stStatBuffer;

	if (FAIL == stat(ccptrPath, &stStatBuffer)) {

		return FALSE;
	}

	return S_ISDIR(stStatBuffer.st_mode);
}

int fnPrintBeginPrint(const CHAR_PTR cptrPath)
{
	int iCount = 0; /* count of printed chars by printf */
	int iCntr = 0; /* generic loop counter */

	iCount = printf("Starting to list from directory %s\n", cptrPath);
	for (iCntr = 0; iCntr < iCount; ++iCntr)
		putchar('-');
	putchar('\n');

	return iCount;
}

int fnListDir(const CHAR_PTR cptrPathToList, const CHAR_PTR cptrExecName)
{
	DIRENT_PTR deptrDirent = NULL;
	DIR_PTR dptrDir = NULL; /* pointer to current dir */
	CHAR_PTR cptrNewPath = NULL;

	int iNumOfSubDirs = 0; /* number of sub-directories of cwd */
	int iAllocLen = 0; /* Length of subdir's path name string */
	int iCntr1; /* generic counters */

	if (!(dptrDir = opendir(cptrPathToList))) {
		perror("Failed to open directory\n");
		printf("dir: %s\n", cptrPathToList);
		return FAIL;
	}

	while ((deptrDirent = readdir(dptrDir))) {
		if ((strcmp(deptrDirent->d_name, CURRENT_DIR)) &&
			(strcmp(deptrDirent->d_name, PARENT_DIR))) {

			printf("%d%5c%-1s/", getpid(), ' ', cptrPathToList);
			printf("%s\n", deptrDirent->d_name);

			if (fnIsDirectory(deptrDirent->d_name)) {
				++iNumOfSubDirs;

				iAllocLen = strlen(cptrPathToList) +
					strlen(deptrDirent->d_name) + 1;

				if (!(cptrNewPath = (CHAR_PTR) malloc
					(sizeof(char) * (iAllocLen + 1)))) {


					perror("Failed to allocate memory "
						"for new path name\n");
					return FAIL;
				}

				strncpy(cptrNewPath, cptrPathToList,
					iCntr1 = strlen(cptrPathToList));

				if (cptrNewPath[iCntr1 - 1] != '/') {
					cptrNewPath[iCntr1] = '/';
					++iCntr1;
				}

				strncpy(cptrNewPath + iCntr1,
					deptrDirent->d_name,
					strlen(deptrDirent->d_name));

				cptrNewPath[iAllocLen] = '\0';


				if ((FAIL == (chdir(cptrNewPath)))
					&& (errno == EACCES)) {

					fnPrintPermDeniedErr(cptrExecName,
						cptrNewPath);

				} else {

					fnListDir(cptrNewPath, cptrExecName);
				}
				free(cptrNewPath);
				chdir(cptrPathToList);

			}


		}
	}


	while ((closedir(dptrDir) == FAIL) && (errno == EINTR));

	return(iNumOfSubDirs);
}

void fnEndPrint(const int iLineWidth)
{
	int iMidint = ((iLineWidth / 2) - 5);
	int iCntr = 0; /* generic counter */

	for (iCntr = 0; iCntr < iLineWidth; ++iCntr)
		putchar('-');
	putchar('\n');
	for (iCntr = 0; iCntr < iMidint; ++iCntr)
		putchar(' ');
	printf("End of list\n");
	return;
}

void fnPrintPermDeniedErr(const CHAR_PTR cptrExecName, const CHAR_PTR cptrPath)
{
	CHAR_PTR cptrErrMsg = NULL;
	int iLen = 0;

	iLen = strlen(cptrExecName) + strlen(cptrPath) + 44;

	cptrErrMsg = (CHAR_PTR) malloc(sizeof(char) * iLen);

	strcpy(cptrErrMsg, cptrExecName);
	iLen = strlen(cptrExecName);

	strcpy((cptrErrMsg + iLen), ": cannot open directory ");
	iLen += strlen(": cannot open directory ");

	strcpy((cptrErrMsg + iLen), cptrPath);
	iLen += strlen(cptrPath);

	cptrErrMsg[iLen] = '\0';

	perror(cptrErrMsg);
	free(cptrErrMsg);

	return;

}

/* End of lsnofork.c */
