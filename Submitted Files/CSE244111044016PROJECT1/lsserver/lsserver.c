/*
 * lsserver.c
 *
 *  Created on: Apr 21, 2013
 *      Author: Mehmet Akif TAŞOVA
 *      Student Number: 111044016
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <sys/signal.h>

#include "lsserver.h"

/*
 * Some unavoidable globals
 */
CLIENTLIST_T clClients;

TIMEVAL tBegin;
FILEDES fdServerClientFifo;
FILEDES fdLogFile;
char strLogFile[S_LOG_NAME_MAX];
pid_t pidParent;

pid_t pidChildList[MAX_CHILD];
int iNumOfChilds;

int main(int argc, CHAR_PTR argv[]) {

	char strLogMsg[BUFSIZ];

	/* The directory path where program begins */
	char strRawClientData[BUFSIZ];
	char strClientPath[PATH_MAX];
	char strClientPid[PID_MAX];

	char cClientCommand = '\0';

	time_t tRawFormat;

	pid_t pidServingChild = 0;
	pid_t pidParentOfClient = 0;

	pidParent = getpid();

	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
	memset(strRawClientData, '\0', BUFSIZ * sizeof(char));
	memset(strClientPath, '\0', PATH_MAX * sizeof(char));
	memset(strClientPid, '\0', PID_MAX * sizeof(char));

	iNumOfChilds = 0;

	clClients.iSize = MAX_CLIENTS;
	clClients.iCurrrent = 0;

	pidParent = getpid();

	if (argc == 2) {
		if (!strcmp(argv[1], "-h")) {
			exit(fnUsage(argv[0]));
		} else if (!strcmp(argv[1], "--help")) {
			exit(fnUsage(argv[0]));
		} else {
			printf("Unrecognized argument: %s\n", argv[1]);
			exit(fnUsage(argv[0]));
		}
	} else if (argc != 1) {
		exit(fnUsage(argv[0]));
	}

	sprintf(strLogFile, "%s%d%s", S_TMP_LOG, pidParent, ".log");
	fdLogFile = open(strLogFile, O_WRONLY | O_APPEND | O_CREAT,
			S_IRUSR | S_IWUSR);

	/*
	 * Set signal handler for SIGINT
	 */
	if (signal(SIGINT, fnSigIntHandler) == SIG_ERR ) {
		sprintf(strLogMsg, "ERROR: Unable to set signal handler for SIGINT\n");
		write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
		memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
		fnExitHandler(EXIT_SIGHANDLER_FAIL);
	}

	if (gettimeofday(&tBegin, NULL )) {
		sprintf(strLogMsg, "ERROR: Failed to get starting time.\n");
		write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
		memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
		fnExitHandler(EXIT_TIME_GET_FAIL);
	}

	sprintf(strLogMsg, "INFO: Starting lsserver...\n");
	write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	time(&tRawFormat);
	sprintf(strLogMsg, "INFO: Starting Time: %s", ctime(&tRawFormat));
	write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	sprintf(strLogMsg, "INFO: Creating FIFO: %s\n", S_FIFO_FILE);
	write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	mkfifo(S_FIFO_FILE, S_FIFO_MODE);

	sprintf(strLogMsg, "INFO: FIFO: %s is ready.\n", S_FIFO_FILE);
	write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	while (1) {

		fdServerClientFifo = open(S_FIFO_FILE, O_RDONLY);

		if (read(fdServerClientFifo, strRawClientData, BUFSIZ * sizeof(char))
				< 0) {
			sprintf(strLogMsg, "ERROR: Error reading FIFO: %s\n",
					strerror(errno));
			write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
			memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
		}
		sscanf(strRawClientData, "%c %s %s %d", &cClientCommand, strClientPid,
				strClientPath, &pidParentOfClient);

		if('C' == cClientCommand){
			sprintf(strLogMsg, "INFO: Client %s Disconnected Time: %s",
					strClientPid, ctime(&tRawFormat));
			write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
			memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
			continue;
		}

		switch (pidServingChild = fork()) {
		case -1:
			sprintf(strLogMsg, "ERROR: failed to call fork()\n");
			write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
			memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
			exit(EXIT_FORK_FAIL);
			break;
		case 0:
			fnDirListChild(strClientPid, strClientPath);
			exit(EXIT_SUCCESS);
			break;
		default:

			if (!fnIsKnownClient(pidParentOfClient)) {
				/* Add new client to list */
				clClients.pidClients[clClients.iCurrrent] = pidParentOfClient;
				(clClients.iCurrrent) = ((clClients.iCurrrent) + 1);
			}

			if (iNumOfChilds < MAX_CHILD) {
				pidChildList[iNumOfChilds] = pidServingChild;
				++iNumOfChilds;

				sprintf(strLogMsg,
						"INFO: Client Connected: serving child: %d client pid:%s dir: %s\n",
						pidServingChild, strClientPid, strClientPath);

				write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));

				fnCheckChilds(); /* clean up child list */

			} else {

				sprintf(strLogMsg,
						"ERROR: Too many children to handle. Killing last one...\n");
				write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
				kill(pidServingChild, SIGINT);

			}

			memset(strRawClientData, '\0', BUFSIZ * sizeof(char));
			memset(strClientPid, '\0', PID_MAX * sizeof(char));
			memset(strClientPath, '\0', PATH_MAX * sizeof(char));
			memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
			break;
		}

	}

	return (EXIT_SUCCESS);
}

int fnUsage(const CHAR_PTR cptrExecName) {

	printf("Usage:\n------\n");
	printf(
			"%s [OPTION]- A Daemon for listing given directory and its sub directories.\n",
			cptrExecName);
	printf("\nOPTION could be:\n");
	printf("-h or --help - Prints this help message.\n");

	return (EXIT_USAGE_PRINT);
}

static void fnSigIntHandler(int iSigNum) {
	char strLogMsg[BUFSIZ];

	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	if (pidParent != getpid()) {
		kill(getppid(), SIGINT);
		return;
	}

	printf("INFO: SIGINT caught, exiting properly...\n");
	fflush(stdout);

	sprintf(strLogMsg, "INFO: SIGINT caught, exiting properly...\n");
	write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	fnExitHandler(EXIT_SIGINT);

}

static void fnExitHandler(int iSigNum) {
	TIMEVAL tEnd;
	long lliElapsedTime = 0;
	char strLogMsg[BUFSIZ];
	int iCntr = 0;
	int iStat;
	time_t tRawFormat;

	while (0 < fnWaitChilds(NULL ))
		;

	if (gettimeofday(&tEnd, NULL )) {
		sprintf(strLogMsg, "ERROR: Failed to get ending time.\n");
		write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
		memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
	}

	close(fdServerClientFifo);
	unlink(S_FIFO_FILE);

	sprintf(strLogMsg, "INFO: FIFO file closed.\n");
	write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	sprintf(strLogMsg, "INFO: Sending Shutdown signal to clients...\n");
	write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	for (iCntr = 0; iCntr < clClients.iCurrrent; ++iCntr) {
		kill(clClients.pidClients[iCntr], SIGUSR1);
	}

	lliElapsedTime = fnTimeDiff(tBegin, tEnd);

	sprintf(strLogMsg, "INFO: Elapsed Time: %ld msec(s)\n", lliElapsedTime);
	write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	time(&tRawFormat);
	sprintf(strLogMsg, "INFO: End Time: %s", ctime(&tRawFormat));
	write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	sprintf(strLogMsg, "----   END OF LOG FILE   ----\n");
	write(fdLogFile, strLogMsg, strlen(strLogMsg) * sizeof(char));
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));
	close(fdLogFile);

	sprintf(strLogMsg, "mv -f %s .", strLogFile);
	/*sprintf(strLogMsg, "cp -f %s .", strLogFile);*/
	system(strLogMsg);
	memset(strLogMsg, '\0', BUFSIZ * sizeof(char));

	/* kill any reamining childs */
	for (iCntr = 0; iCntr < iNumOfChilds; ++iCntr) {
		waitpid(pidChildList[iCntr], &iStat, WNOHANG);
		if (!WIFEXITED(iStat)) {
			kill(pidChildList[iCntr], SIGINT);
		}
	}

	exit(iSigNum);
}

long fnTimeDiff(TIMEVAL tBegin, TIMEVAL tEnd) {
	return ((MILLION * (tEnd.tv_sec - tBegin.tv_sec))
			+ (tEnd.tv_usec - tBegin.tv_usec));
}

int fnListDir(const CHAR_PTR cptrPathToList, FILEDES fdFIFO) {

	DIRENT_PTR deptrDirent = NULL;
	DIR_PTR dptrDir = NULL; /* pointer to current dir */
	char cptrNewPath[BUFSIZ];

	int iNumOfSubDirs = 0; /* number of sub-directories of cwd */

	char strPrintLine[LD_LINE_LEN];
	int iPrintLine = 0;
	char strMsg[BUFSIZ];

	memset(strPrintLine, '\0', LD_LINE_LEN * sizeof(char));
	memset(strMsg, '\0', BUFSIZ * sizeof(char));
	memset(cptrNewPath, '\0', BUFSIZ * sizeof(char));

	if (!(dptrDir = opendir(cptrPathToList))) {
		sprintf(strMsg, "WARN: Child %d: Failed to open directory: %s : %s\n",
				getpid(), cptrPathToList, strerror(errno));
		fnPrintError(strMsg);
		memset(strMsg, '\0', BUFSIZ * sizeof(char));
		return (iNumOfSubDirs);
	}

	while ((deptrDirent = readdir(dptrDir))) {
		if ((strcmp(deptrDirent->d_name, CURRENT_DIR))
				&& (strcmp(deptrDirent->d_name, PARENT_DIR))) {

			if (cptrPathToList[strlen(cptrPathToList) - 1] != '/') {
				strcat(cptrPathToList, "/");
			}

			/* Write found files to FIFO */
			sprintf(strPrintLine, "%s%s\n", cptrPathToList,
					deptrDirent->d_name);
			iPrintLine = (strlen(strPrintLine) * sizeof(char));
			write(fdFIFO, strPrintLine, iPrintLine);

			memset(cptrNewPath, '\0', BUFSIZ * sizeof(char));

			sprintf(cptrNewPath, "%s%s", cptrPathToList, deptrDirent->d_name);

			if (fnIsDirectory(cptrNewPath)) {
				++iNumOfSubDirs;

				if ((FAIL == (chdir(cptrNewPath))) && (errno == EACCES)) {

					sprintf(strMsg,
							"WARN: Child %d: Cannot open directory: %s: %s\n",
							getpid(), cptrNewPath, strerror(errno));
					fnPrintError(strMsg);
					memset(strMsg, '\0', BUFSIZ * sizeof(char));

				} else {

					chdir(cptrNewPath);
					iNumOfSubDirs += fnListDir(cptrNewPath, fdFIFO);
				}
				/*free(cptrNewPath);*/
				chdir(cptrPathToList);

			} else {
				continue;
			}

		}
	}

	while ((closedir(dptrDir) == FAIL) && (errno == EINTR))
		;

	return (iNumOfSubDirs);
}

void fnPrintError(const CHAR_PTR cptrMessage) {

	write(fdLogFile, cptrMessage, strlen(cptrMessage) * sizeof(char));

	return;

}

int fnIsDirectory(const CHAR_PTR ccptrPath) {
	STAT stStatBuffer;

	if (FAIL == stat(ccptrPath, &stStatBuffer)) {

		return 0;
	}

	return S_ISDIR(stStatBuffer.st_mode);
}

void fnDirListChild(const CHAR_PTR cptrClientPid, const CHAR_PTR cptrPathToList) {

	FILEDES fdCleintServer;
	char strPath[PATH_MAX];
	char strMsg[BUFSIZ];
	TIMEVAL tStart, tDone;
	long lliElapsedTime = 0L;

	memset(strPath, '\0', PATH_MAX * sizeof(char));
	memset(strMsg, '\0', BUFSIZ * sizeof(char));

	sprintf(strMsg, "INFO: Client Connected. PID: %s\n", cptrClientPid);
	write(STDOUT_FILENO, strMsg, sizeof(strMsg));
	write(fdLogFile, strMsg, sizeof(strMsg));
	memset(strMsg, '\0', BUFSIZ * sizeof(char));

	if (gettimeofday(&tStart, NULL )) {
		sprintf(strMsg, "ERROR: Failed to get beginnig time. pid: %d\n",
				getpid());
		write(fdLogFile, strMsg, strlen(strMsg) * sizeof(char));
		exit(EXIT_TIME_GET_FAIL);
	}

	sprintf(strPath, SC_FIFO_BASE, atoi(cptrClientPid), 0);
	fdCleintServer = open(strPath, O_WRONLY);

	fnListDir(cptrPathToList, fdCleintServer);

	close(fdCleintServer);

	if (gettimeofday(&tDone, NULL )) {
		sprintf(strMsg, "ERROR: Failed to get ending time. pid: %d\n",
				getpid());
		write(fdLogFile, strMsg, strlen(strMsg) * sizeof(char));
		exit(EXIT_TIME_GET_FAIL);
	}

	lliElapsedTime = fnTimeDiff(tStart, tDone);

	sprintf(strMsg, "INFO: Child %d done serving. Time: %ld msec(s)\n",
			getpid(), lliElapsedTime);
	write(fdLogFile, strMsg, strlen(strMsg) * sizeof(char));
	memset(strMsg, '\0', BUFSIZ * sizeof(char));

	exit(EXIT_SUCCESS);
}

pid_t fnWaitChilds(INT_PTR iptrStatLoc) {
	int iRetval;

	while (((iRetval = wait(iptrStatLoc)) == -1) && (errno == EINTR))
		;
	return iRetval;
}

void fnCheckChilds(void) {
	int iCntr = 0;
	int iStat;

	for (iCntr = 0; iCntr < iNumOfChilds; ++iCntr) {
		waitpid(pidChildList[iCntr], &iStat, WNOHANG);
		if (WIFEXITED(iStat)) {
			if (MAX_CLIENTS > iCntr) {
				if (MAX_CLIENTS == (iCntr + 1)) {
					pidChildList[iCntr] = 0;
				} else {
					pidChildList[iCntr] = pidChildList[iCntr + 1];
				}
			}
		}
	}
}

int fnIsKnownClient(const pid_t pidClient) {
	int iCurr = clClients.iCurrrent;
	int iCntr = 0;

	for (iCntr = 0; iCntr < iCurr; ++iCntr) {
		if (pidClient == clClients.pidClients[iCntr]) {
			return 1;
		}
	}
	return 0;
}
/* End of lsserver.c */
