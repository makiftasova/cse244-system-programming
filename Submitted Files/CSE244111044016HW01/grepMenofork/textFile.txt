When a proccess creates a child, both parent and child proceed with execution
from the point of the fork. The parent can execute wait or waitpid to block
until child finishes.
