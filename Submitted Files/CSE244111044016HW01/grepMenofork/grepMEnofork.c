/* 
 * File:   grepMEnofork.c
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * CSE244 - System Programming HW01-Part1
 *
 * Created on March 19, 2013, 6:59 PM
 */


#include <string.h>
#include <ctype.h>
#include <sys/time.h>
#include <unistd.h>

#include "grepMEnofork.h"

/*
 * 
 */
int main(int argc, CHRPTR argv[])
{
	UNSIGINT uWordCount = 0; /* Total number of words to search */
	UNSIGINT uCntr = 0; /* Loop Counter */

	/* Total number of characters whose contained in file */
	UNSIGINT uTotalNumOfChars = 0;

	FILEPTR fptrFile = NULL;
	CHRPTR cptrFileName = NULL;
	CHRPTR cptrFileBuffer = NULL;

	/* Time calculations part 1*/
	TIMEVAL tvStart, tvEnd;
	long lMtime, lSecs, lUsecs;

	gettimeofday(&tvStart, NULL);

	/* End of Time calculations part 1*/

	/* If file name input is illegal */
	if ((argc > 1) && ('-' != argv[1][0])) {
		if (fnUsage(STATUS_NO_FILNE_NAME, argv[0]))
			return 0;
	}

	/* If not enough arguments given */
	if (argc < 3) {
		if (fnUsage(STATUS_NOT_ENOUGH_ARGS, argv[0]))
			return 0;
	}


	cptrFileName = (CHRPTR) malloc(sizeof (char) * strlen(argv[1]));
	strcpy(cptrFileName, &(argv[1][1]));

	if (NULL == (fptrFile = fopen(cptrFileName, "r"))) {

		printf("ERROR: Unable to open file %s\n", cptrFileName);
		printf("Please check File and/or Permissions.\n");
		free(cptrFileName); /* free this memory block */
		return (EXIT_FILE_NOT_OPEN);
	}



#ifdef DEBUG
	printf("[DBG] File %s Successfully Open\n", cptrFileName);
#endif

	/* If flow comes here, there is no need for name of file anymore */
	free(cptrFileName);
	cptrFileName = NULL; /* For more safety */

	uTotalNumOfChars = fnCharCount(fptrFile);

	cptrFileBuffer =
		(CHRPTR) malloc(sizeof (char) * (uTotalNumOfChars));

	/* Copy ingredients of file into a Buffer */
	fnCopyFileToBuffer(fptrFile, cptrFileBuffer, uTotalNumOfChars);

	fclose(fptrFile); /* Close file */
	fptrFile = NULL; /* For more safety */

#ifdef DEBUG
	printf("[DBG] Read Text:\n%s", cptrFileBuffer);
	printf("[DBG] End of read text\n");
#endif
	/* End Of Read File Part */


	uCntr = 0;
	uWordCount = (argc - 2);

	for (uCntr = 0; uCntr < uWordCount; ++uCntr) {
		printf("Number of \"%s\" : %d\n",
		argv[uCntr + 2], fnCountWord(argv[uCntr + 2], cptrFileBuffer));
	}


	/* Return Buffer to OS then exit*/
	if (NULL != cptrFileBuffer) {
		free(cptrFileBuffer); /* free memory of File Buffer */
		cptrFileBuffer = NULL; /* For more safety */
	}

	/* Beginning of run time calculations */
	gettimeofday(&tvEnd, NULL);
	lSecs = (tvEnd.tv_sec - tvStart.tv_sec);
	lUsecs = (tvEnd.tv_usec - tvStart.tv_usec);
	lMtime = (((lSecs * 1000) + (lUsecs / 1000)) + 0.5);
	printf("End of Proccess. pid: %d Elapsed time = %ld msec(s)\n",
	getpid(),
	lMtime);
	/* End of run time calculations */

	return (EXIT_SUCCESS);
}

int fnUsage(int iStatus, CHRPTR cptrFirstArg)
{
	int iReturnStatus = 0;

	switch (iStatus) {
	case STATUS_NO_FILNE_NAME:
		printf("ERROR: No File given to read or Argument misplaced!\n");
		iReturnStatus = 1;
		break;
	case STATUS_NOT_ENOUGH_ARGS:
		printf("ERROR: Not enough arguments given!\n");
		iReturnStatus = 1;
		break;
	default:
		printf("ERROR: Unknown error occurred!\n");
		printf("ERROR CODE: %d\n", iStatus);
		iReturnStatus = 0;
		break;
	}

	printf("\nUsage:\n------\n");
	printf("%s -[fileName] \"[WORD1]\" \"[WORD2]\" ...\n", cptrFirstArg);

	return iReturnStatus;
}

UNSIGINT fnCharCount(FILEPTR fptrFile)
{
	char chrBuf = 0;
	UNSIGINT uTotalNumOfChars = 0;
	int iJunk = 0;

#ifdef DEBUG
	printf("[DBG] Counting characters in file\n");
#endif
	while (!feof(fptrFile)) {
		iJunk = fscanf(fptrFile, "%c", &chrBuf);
		++uTotalNumOfChars;
	}
#ifdef DEBUG
	printf("[DBG] Total number of chars: %d\n", uTotalNumOfChars);
#endif

	/* Return to beginning of file */
	rewind(fptrFile);

	return uTotalNumOfChars;
}

int fnCopyFileToBuffer(FILEPTR fptrFile, CHRPTR cptrBuffer, size_t uNumOfChars)
{
	UNSIGINT uCntr = 0;

	for (uCntr = 0; uCntr < (uNumOfChars - 1); ++uCntr) {
		cptrBuffer[uCntr] = tolower(fgetc(fptrFile));
		if (EOF == cptrBuffer[uCntr])
			break;
	}

	/* Make Sure String is terminated*/
	cptrBuffer[uCntr] = CHAR_NULL;

	return uCntr;
}

UNSIGINT fnCountWord(const CHRPTR strWord, const CHRPTR cptrStream)
{
	UNSIGINT uStreamLen = 0; /* Length of Stream */
	UNSIGINT uWordLen = 0; /* Length of word to search */
	UNSIGINT uCntrStream = 0; /* counter for index of stream */
	UNSIGINT uCntrWord = 0; /* counter for number of found words */
	UNSIGINT uCntr = 0; /* basic loop counter */

	CHRPTR strSearchWord = NULL;

	uStreamLen = strlen(cptrStream);
	uWordLen = strlen(strWord);

	strSearchWord = (CHRPTR) malloc(sizeof (char) * (uWordLen + 1));

	for (uCntr = 0; uCntr < uWordLen; ++uCntr) {
		strSearchWord[uCntr] = tolower(strWord[uCntr]);
	}

	strSearchWord[uCntr] = CHAR_NULL;

	for (uCntrStream = 0; uCntrStream < uStreamLen; ++uCntrStream) {
		if (!strncmp(strSearchWord,
			&(cptrStream[uCntrStream]), uWordLen)) {
			++uCntrWord;
			uCntrStream += (uWordLen - 1);
			continue;
		}
	}


	free(strSearchWord);

	return uCntrWord;
}

/* End of grepMEnofork.c */
