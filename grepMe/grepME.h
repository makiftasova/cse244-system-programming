/* 
 * File:   grepME.h
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * CSE244 - System Programming HW01-Part2
 *
 * Created on March 19, 2013, 6:59 PM
 */


#ifndef GREPME_H
#define GREPME_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>
#include <errno.h>
#include <sys/time.h>



/* Exit Codes */
#define EXIT_FILE_NOT_OPEN -1

/* Status codes for usage */
#define STATUS_NO_FILNE_NAME 1
#define STATUS_NOT_ENOUGH_ARGS 2

/* char NULL for terminating strings */
#define CHAR_NULL '\0'

/* type definition for char pointer */
typedef char* CHRPTR;

/* type definition for int pointer */
typedef int* INTPTR;

/* type definition for unsigned int */
typedef unsigned int UNSIGINT;

/* type definition for File pointer */
typedef FILE* FILEPTR;

/* type definition for pid_t pointer. used for creating array of pid_t values */
typedef pid_t* PIDPTR;

/* timeval struct for runtime calculations */
typedef struct timeval TIMEVAL;

/* 
 * Prints usage by given status code
 * 
 * iStatus - Status code
 * cptrFirstArg - A string which contains executable name (argv[0] from main)
 */
int fnUsage(int iStatus, CHRPTR cptrFirstArg);

/* 
 * Counts all characters in given file by reading them one by one. 
 * When counting done, rewinds file to beginning
 * 
 * fptrFile FILE* of file
 */
UNSIGINT fnCharCount(FILEPTR fptrFile);

/* 
 * Reads every single character in given file to given buffer. 
 * Memory for buffer must be allocated before sending 
 *	pointer of buffer to function.
 * 
 * Returns total number of read characters from file
 * 
 * fptrFile - FILE* of file
 * cptrBuffer - Buffer to read
 * uNumOfChars - Number of characters to read from file to buffer
 */
int fnCopyFileToBuffer(FILEPTR fptrFile, CHRPTR cptrBuff, UNSIGINT uNumOfChars);

/* 
 * Counts given word in given Stream
 * 
 * strWord - Word to count
 * cptrStream - Steam to search
 */
UNSIGINT fnCountWord(const CHRPTR strWord, const CHRPTR cptrStream);

/* 
 * Function for waiting all child proccess to terminate
 */
pid_t fnR_wait(INTPTR iptrStatLoc);

#endif

/* End of grepME.h */