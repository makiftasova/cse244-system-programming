/* 
 * File:   MYmore.h
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * A simple clone of well-known "more" command
 * 
 */


#ifndef MAT_MYMORE
#define MAT_MYMORE

#include <termios.h>
#include <sys/ioctl.h>

#define DEFAULT_LINE_PER_SCREEN 24
#define DEAFULT_LINE_WIDTH 80

/* Exit code which will return by usage function*/
#define EXIT_USAGE_PRINT 1
#define EXIT_FILE_OPEN_FAIL 2
#define EXIT_FILE_BUFF_ERROR 3
#define EXIT_NOT_TERMINAL 4


/* Some useful char values */
#define CHAR_NEW_LINE '\n'
#define CHAR_SPACE ' '
#define CHAR_NULL '\0'

/* Type definition from boolean type */
typedef enum {
    FALSE = 0, TRUE = 1
} BOOL_T;

/* typedef for standard C identifiers */
typedef char* CHRPTR;

typedef int* INTPTR;

typedef unsigned int UNSIGINT;

/* typedef for FILE pointer */
typedef FILE* FILEPTR;

/* termios.h related typedefs*/
typedef struct termios TERMIOS_T;

/* ioctl.h related typedefs*/
typedef struct winsize WINSIZE_T;


/*
 * Prints usage of program by given executable name 
 *  (in most situations this is argv[0]).
 * 
 *  cptrExecName Current name of programs executable name.
 */
void fnUsage(const CHRPTR ccptrExecName);

/* 
 * Counts all characters in given file by reading them one by one. 
 * When counting done, rewinds file to beginning
 * 
 * fptrFile FILE* of file
 */
UNSIGINT fnCharCount(FILEPTR fptrFile);

/* 
 * Counts lines in given file by reading them one by one. 
 * When counting done, rewinds file to beginning
 * 
 * fptrFile FILE* of file
 */
UNSIGINT fnLineCount(FILEPTR fptrFile);

/* 
 * Reads every single character in given file to given buffer. 
 * Memory for buffer must be allocated before sending 
 *	pointer of buffer to function.
 * 
 * Returns total number of read characters from file
 * 
 * fptrFile - FILE* of file
 * cptrBuffer - Buffer to read
 * uNumOfChars - Number of characters to read from file to buffer
 */
int fnCopyFileToBuffer(FILEPTR fptrFile, CHRPTR cptrBuff, UNSIGINT uNumOfChars);

/*
 * Takes text in cptrBuff and prints uLineCount line of it where one line is
 *   uLineWidth characters width. Returns number of printed characters
 * 
 * cptrBuff Text Buffer of File.
 * uLineCount How many lines will be printed.
 * uLineWidth Width of a line.
 */
UNSIGINT fnPrintLines(const CHRPTR cptrBuff, const UNSIGINT uBuffLen,
                      const UNSIGINT uLineCount, const UNSIGINT uLineWidth);

/*
 * Returns given file streams width value
 * 
 * iFileDes File descriptor to stream to get width value
 */
int fnGetStreamWidth(int iFileDes);

/*
 * Reads a single char from given File Descriptor, then returns it
 * 
 * iFileDes File Descriptor of input stream
 */
char fnReadChar(int iFileDes);

#endif

/* End of MYmore.h */
