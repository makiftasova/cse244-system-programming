/* 
 * File:   MYmore.c
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * A simple clone of well-known "more" command
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

/* MYmore specific header */
#include "MYmore.h"

int main(int argc, CHRPTR argv[])
{
	/* How many lines will be printed per screenful */
	UNSIGINT uLinePerScrn = DEFAULT_LINE_PER_SCREEN;
	/* A temp variable for validate check for reading number of lines */
	UNSIGINT uReadCheck = 0;

	/* Default width per Line */
	UNSIGINT uLineWidth = DEAFULT_LINE_WIDTH;

	/* 
	 * Default filename index. indicates position of given filename in 
	 * argv[] (argument array)
	 */
	UNSIGINT uFIndx = 1;

	/* Pointer of file to read */
	FILEPTR fptrReadFile = NULL;

	/* A copy of given input file in memory */
	CHRPTR cptrFileBuff = NULL;

	/* Number of characters in given file */
	UNSIGINT uCharCount = 0;

	/* Current printing position */
	UNSIGINT uPos = 0;

	/* User's choice of scroll ENTER is 1 line, SPACE is N lines*/
	char cUserCommand = 0;

	/* Check STDOUT is a terminal */
	if (!isatty(STDOUT_FILENO)) {
		fprintf(stderr, "STDOUT file is not a terminal.\n");
		return(EXIT_NOT_TERMINAL);
	}

	/* Check STDIN is a terminal */
	if (!isatty(STDIN_FILENO)) {
		fprintf(stderr, "STDIN file is not a terminal.\n");
		return(EXIT_NOT_TERMINAL);
	}

	/* Get current line width of STDOUT */
	uLineWidth = fnGetStreamWidth(STDOUT_FILENO);

	/* 
	 * If there is a usage error, print usage and exit, 
	 * else start acting like more
	 */
	if ((argc < 2) || (argc > 3)) {
		fnUsage(argv[0]);
		return(EXIT_USAGE_PRINT);
	} else if (argc == 3) {
		uReadCheck = atoi(argv[1]);
		if ((uReadCheck < 1) || (uReadCheck > 24)) {
			fnUsage(argv[0]);
			return(EXIT_USAGE_PRINT);
		}
		uLinePerScrn = uReadCheck;
		++uFIndx;
		;
	}

	if (!(fptrReadFile = fopen(argv[uFIndx], "r"))) {
		printf("%s: %s: %s\n", argv[0], argv[uFIndx], strerror(errno));
		return(EXIT_FILE_OPEN_FAIL);
	}

	/* 
	 * Count number of characters in input file, for allocating memory for 
	 * buffer of input file
	 */
	uCharCount = fnCharCount(fptrReadFile);

	/* Allocate buffer for input file */
	if (!(cptrFileBuff = (CHRPTR) malloc(sizeof(char) * uCharCount))) {
		printf("Failed to Allocate Memory for Buffering File\n");
		printf("%s\n", strerror(errno));
		return(EXIT_FILE_BUFF_ERROR);
	}

	/* Copy whole file into file buffer */
	fnCopyFileToBuffer(fptrReadFile, cptrFileBuff, uCharCount);

	/* We are done with file */
	fclose(fptrReadFile);

	/* ** Let the fun begin ** */

	/* Print first N line of file */
	uPos = fnPrintLines(cptrFileBuff, uCharCount, uLinePerScrn, uLineWidth);

	/* Print rest of line by user's command */
	while (TRUE) {
		/* Check for EOF */
		if (uPos >= uCharCount)
			break;

		/* Get user's command */
		cUserCommand = fnReadChar(STDIN_FILENO);

		/* Process user's command */
		if (cUserCommand == CHAR_NEW_LINE) {
			uPos += fnPrintLines(&cptrFileBuff[uPos],
				(uCharCount - uPos), 1, uLineWidth);
		} else if (cUserCommand == CHAR_SPACE) {
			uPos += fnPrintLines(&cptrFileBuff[uPos],
				(uCharCount - uPos), uLinePerScrn, uLineWidth);
		}
	} /* End of printing part */
	/* **End of fun part ** */

	/* Cleaning mess */
	free(cptrFileBuff);
	return(EXIT_SUCCESS);
}

void fnUsage(const CHRPTR cptrExecName)
{
	printf("usage:\n------\n%s [LINECOUNT] [FILE]\n", cptrExecName);
	printf("With no LINECOUNT prints 24 lines per screenful\n");
	printf("LINE COUNT must be in range [1....24]\n");
	printf("ENTER prints one more line of FILE\n");
	printf("SPACE prints LINECOUNT line of FILE\n");
}

UNSIGINT fnCharCount(FILEPTR fptrFile)
{
	char chrBuf = 0;
	UNSIGINT uTotalNumOfChars = 0;

	while (!feof(fptrFile)) {
		fscanf(fptrFile, "%c", &chrBuf);
		++uTotalNumOfChars;
	}

	/* Return to beginning of file */
	rewind(fptrFile);

	return uTotalNumOfChars;
}

UNSIGINT fnLineCount(FILEPTR fptrFile)
{
	char chrBuf = 0;
	UNSIGINT uTotalNumOfLines = 0;

	while (!feof(fptrFile)) {
		fscanf(fptrFile, "%c", &chrBuf);
		if ((CHAR_NEW_LINE == chrBuf) || (EOF == chrBuf))
			++uTotalNumOfLines;
	}

	/* Return to beginning of file */
	rewind(fptrFile);

	return(uTotalNumOfLines + 1);
}

int fnCopyFileToBuffer(FILEPTR fptrFile, CHRPTR cptrBuff, UNSIGINT uNumOfChars)
{
	UNSIGINT iCntr = 0;

	for (iCntr = 0; iCntr < (uNumOfChars - 1); ++iCntr) {
		cptrBuff[iCntr] = fgetc(fptrFile);
		if (EOF == cptrBuff[iCntr])
			break;
	}

	/* Make Sure String is terminated*/
	cptrBuff[iCntr] = CHAR_NULL;

	return iCntr;
}

UNSIGINT fnPrintLines(const CHRPTR cptrBuff, const UNSIGINT uBuffLen,
	const UNSIGINT uLineCount, const UNSIGINT uLineWidth)
{
	UNSIGINT uLinesPrint = 0;
	UNSIGINT uIndex = 0;
	UNSIGINT uCharsPrint = 0;
	char cToPrint = 0;

	while (uIndex < uBuffLen) {
		cToPrint = cptrBuff[uIndex++];
		++uCharsPrint;
		putchar(cToPrint);
		if (CHAR_NEW_LINE == cToPrint) {
			uCharsPrint = 0;
			++uLinesPrint;
		} else if (0 == (uCharsPrint % (uLineWidth))) {
			uCharsPrint = 0;
			++uLinesPrint;
			putchar(CHAR_NEW_LINE); /* Finish current line */
		}

		if (uLinesPrint >= uLineCount)
			break;
	}

	return uIndex;
}

int fnGetStreamWidth(int iFileDes)
{
	WINSIZE_T wWinsize;
	ioctl(iFileDes, TIOCGWINSZ, &wWinsize);

	return(wWinsize.ws_col);
}

char fnReadChar(int iFileDes)
{
	char cReadChar = 0;
	TERMIOS_T tActive, tBackup;

	tcgetattr(iFileDes, &tBackup);
	tcgetattr(iFileDes, &tActive);

	/* Make active configuration of terminal non-canonical */
	tActive.c_lflag &= ~(ICANON | ECHO | ECHOE | ECHOK | ECHONL | ICRNL);

	tcsetattr(iFileDes, TCSANOW, &tActive);

	read(iFileDes, &cReadChar, sizeof(char));

	tcsetattr(iFileDes, TCSANOW, &tBackup);

	return cReadChar;
}

/* End of MYmore.c */