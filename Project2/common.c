/*
 * common.c
 *
 * CSE244 - Project 2 - Common Functions
 *
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 */

#include "common.h"

int write_log(FILE_PTR logFile, const CHAR_PTR message) {
	time_t rawtime;
	time(&rawtime);
	return fprintf(logFile, "%s #### TIME: %s\n", message, ctime(&rawtime));
}

size_t indexOf(const CHAR_PTR str, const char ch) {
	size_t index = 0;
	while (str[index] != '\0') {
		if (str[index] == ch) {
			break;
		}
		++index;
	}
	return index;
}
