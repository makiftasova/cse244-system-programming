/*
 * types.h
 *
 * CSE244 - Project 2 - Common types and defines
 *
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 */

#ifndef MAT_TYPES_H_
#define MAT_TYPES_H_

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/* Some useful size defines */
#ifndef BUFSIZ
#define BUFSIZ 8192
#endif

#ifndef LINE_MAX
#define LINE_MAX 2048
#endif

#define IP_LEN 16

/* Ports */
#define TIMER_PORT 2222 /* Listen port of timer server. Comes from GYTE */
#define MATH_PORT 6666 /* Listen port for Math Server */

/* Lengths */
#define IP_LEN 16 /* Length of an IPV4 Address */
#define MATRIX_LEN 6 /* One dimension of matrix */

/* Max support limits */
#define MAX_CLIENTS_SUPPORT 1024 /* Maximum number of possible clients */

/* Localhost IP */
#define LOCALHOST "127.0.0.1"

/* Common exit codes */
#define EXIT_USAGE 1

#define EXIT_SOCK_LISTEN_FAIL 2

#define EXIT_SOCK_BIND_FAIL 3

#define EXIT_SOCK_CONN_FAIL 4

#define EXIT_SIGINT 5

#define EXIT_SIGHANDLER_FAIL 6

/* shutdown codes for socket */
#define SHUTDOWN_RECV 0
#define SHUTDOWN_SEND 1
#define SHUTDOWN_ALL 2

/* Required for time calculations */
#define MILLION 1000000L
#define BILLION 1000000000L

/* Basic communication codes which will be used by Tick Server */
#define SERVER_DIE  0
#define SERVER_TICK 1
#define SERVER_ASSGN_ID 2

/* Required typedefs */
typedef void * VOID_PTR;

typedef unsigned int UINT_T;

typedef char * CHAR_PTR;

typedef FILE * FILE_PTR;

typedef int SOCKET_DES; /* Socket Descriptor Type */

typedef struct sockaddr SOCK_SERVER_ADDR;

typedef struct sockaddr_in SOCK_CLIENT_ADDR;

typedef struct timespec TIMESPEC;

typedef struct sigaction SIGACTION;

/* A structure stores connection info */
typedef struct {
	SOCKET_DES sock; /* Client specific socket */
	SOCK_CLIENT_ADDR* client_addr; /* info of client */
	long res_msec; /* Time resolution as milliseconds */
} CON_INFO_T;

typedef struct {
	UINT_T size;
	CON_INFO_T list[MAX_CLIENTS_SUPPORT];
} CLIENT_LIST_T;

typedef struct {
	int height;
	int width;
	float matrix[MATRIX_LEN][MATRIX_LEN];
} MATRIX_T;

#endif /* TYPES_H_ */
