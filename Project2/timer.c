/*
 * timer.c
 *
 * CSE244 - Project 2 - Tick Server
 *
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 */

#include "common.h"
#include "timer.h"

const size_t CLI_ADDR_SIZE = sizeof(SOCK_CLIENT_ADDR);
const size_t SER_ADDR_SIZE = sizeof(SOCK_SERVER_ADDR);

volatile SOCKET_DES sockListen;
volatile int loopCond; /* generic infinite loop condition */

const int TICK = SERVER_TICK;

pthread_t threadSockListener;

TIMER_CLIENTS_T clients;
long timeDiff;

FILE_PTR logFile;

static sigset_t thread_sigmask;

pthread_mutex_t logFile_mutex = PTHREAD_MUTEX_INITIALIZER;

/*
 * Main Function
 */
int main(int argc, CHAR_PTR argv[]) {

	size_t i = 0;
	long tmpTimeDiff = 0;
	TIMESPEC now;
	int bytes_send = 0;
	char logBuff[BUFSIZ];

	TIMESPEC waitLen;

	memset(logBuff, '\0', sizeof(char) * BUFSIZ);

	clients.size = 0;
	loopCond = 1;

	logFile = fopen(TICKER_LOG_FILE, "w");

	sprintf(logBuff, "INFO: Starting server at port %d", TIMER_PORT);
	write_log(logFile, logBuff);
	memset(logBuff, '\0', sizeof(char) * BUFSIZ);

	if (2 != argc) {
		fnUsage(argv[0]);
		return (EXIT_USAGE);
	}
	timeDiff = atol(argv[1]);
	if (99 >= timeDiff) {
		printf("Illegal Format for Resolution: %s\n", argv[1]);
		fnUsage(argv[0]);
		return (EXIT_ILLEGAL_RESOLUTION);
	}

	sigemptyset(&thread_sigmask);
	sigaddset(&thread_sigmask, SIGINT);
	if (pthread_sigmask(SIG_BLOCK, &thread_sigmask, NULL ) != 0) {
		write_log(logFile, "ERR: Failed to install SIGHANDLER.");
		return (EXIT_SIGHANDLER_FAIL);
	}

	/* ignore SIGPIPE */
	if (signal(SIGPIPE, SIG_IGN ) == SIG_ERR ) {
		write_log(logFile, "ERR: Failed to install SIGHANDLER.");
		return (EXIT_SIGHANDLER_FAIL);
	}

	write_log(logFile, "INFO: Starting socket listener.");

	/* client listener begin */
	pthread_create(&threadSockListener, NULL, fnClientListener, NULL );

	/* apply SIGINT handler */
	if (pthread_sigmask(SIG_UNBLOCK, &thread_sigmask, NULL ) != 0) {
		return (EXIT_SIGHANDLER_FAIL);
	}
	if (signal(SIGINT, fnSigIntHandler) == SIG_ERR ) {
		return (EXIT_SIGHANDLER_FAIL);
	}
	pthread_mutex_lock(&logFile_mutex);
	write_log(logFile, "INFO: Installing SIGHANDLERs.");
	pthread_mutex_unlock(&logFile_mutex);

	/* ticker begin */

	waitLen.tv_sec = 0L;
	waitLen.tv_nsec = (long) timeDiff;

	pthread_mutex_lock(&logFile_mutex);
	write_log(logFile, "INFO: Starting Ticker.");
	pthread_mutex_unlock(&logFile_mutex);

	while (loopCond) {
		clock_gettime(CLOCK_MONOTONIC_RAW, &now);
		for (i = 0; i < clients.size; ++i) {
			if (clients.clients[i].online) {
				tmpTimeDiff = fnTimeDiffLong(&(clients.clients[i].lastTick),
						&now);
				if (tmpTimeDiff
						>= ((clients.clients[i].tickResMicro) * MILLION)) {
					bytes_send = send(clients.clients[i].sock, &TICK,
							sizeof(int), 0);

					clients.clients[i].lastTick = now;

					if (bytes_send == 0) {
						clients.clients[i].online = 0;
						close(clients.clients[i].sock);
					}
				}
			} else {
				continue;
			}
		}

		nanosleep(&waitLen, NULL );

	}

	return (EXIT_SUCCESS);

}

void fnUsage(CHAR_PTR execName) {
	char strMsg[128];

	sprintf(strMsg,
			"%s <nanoseconds>\n\n<nanoseconds>  Resolution of Timer server\n",
			execName);
	write(STDOUT_FILENO, strMsg, (sizeof(char) * strlen(strMsg)));
}

int fnAddClient(TIMER_CLIENTS_T* list, const TICKDATA_T client) {
	if (list->size >= (MAX_CLIENTS_SUPPORT)) {
		return 0; /* list is full */
	}
	list->clients[list->size] = client;
	++(list->size);
	return 1;
}

VOID_PTR fnClientListener(VOID_PTR args) {

	SOCK_CLIENT_ADDR serverAddr;
	TIMESPEC now;
	int clientId;
	char logBuff[BUFSIZ];

	memset(logBuff, '\0', sizeof(char) * BUFSIZ);

	clients.size = 0;

	sockListen = socket(AF_INET, SOCK_STREAM, 0);

	if (sockListen < 0) {
		perror("Failed to create listen socket");
		pthread_mutex_lock(&logFile_mutex);
		write_log(logFile, "ERR: Failed to create listen socket.");
		pthread_mutex_unlock(&logFile_mutex);
		exit(-1);
	}

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(TIMER_PORT);
	serverAddr.sin_addr.s_addr = inet_addr(LOCALHOST);
	memset(&(serverAddr.sin_zero), '0', 8);

	if (bind(sockListen, (SOCK_SERVER_ADDR*) &serverAddr,
			sizeof(SOCK_SERVER_ADDR)) == -1) {
		perror("bind fail");
		pthread_mutex_lock(&logFile_mutex);
		write_log(logFile, "ERR: Failed to Bind Port. Exiting...");
		pthread_mutex_unlock(&logFile_mutex);
		kill(SIGINT, getpid());
	}

	if (listen(sockListen, MAX_CLIENTS_SUPPORT) == -1) {
		perror("listen fail");
		pthread_mutex_lock(&logFile_mutex);
		write_log(logFile, "ERR: Failed to Listen Port. Exiting...");
		pthread_mutex_unlock(&logFile_mutex);
		kill(SIGINT, getpid());
	}

	while (loopCond) {

		if (clients.size >= MAX_CLIENTS_SUPPORT) {
			pthread_mutex_lock(&logFile_mutex);
			write_log(logFile, "ERR: Client List overflow. Exiting...");
			pthread_mutex_unlock(&logFile_mutex);
			kill(SIGINT, getpid());
			break;
		}

		clients.clients[clients.size].sin_size = sizeof(SOCK_CLIENT_ADDR);

		if ((clients.clients[clients.size].sock = accept(sockListen,
				(SOCK_SERVER_ADDR*) clients.clients[clients.size].client_addr,
				&(clients.clients[clients.size].sin_size))) == -1) {

			perror("accept fail"); /* TODO log */
			pthread_mutex_lock(&logFile_mutex);
			write_log(logFile, "WARN: Failed to accept new connection.");
			pthread_mutex_unlock(&logFile_mutex);
			continue;
		}

		clientId = clients.size + SERVER_ASSGN_ID;
		send(clients.clients[clients.size].sock, &clientId, sizeof(int), 0);

		recv(clients.clients[clients.size].sock,
				&(clients.clients[clients.size].tickResMicro), sizeof(long), 0);

		send(clients.clients[clients.size].sock, &TICK, sizeof(int), 0);

		clock_gettime(CLOCK_MONOTONIC_RAW, &now);
		clients.clients[clients.size].lastTick = now;

		clients.clients[clients.size].online = 1;

		sprintf(logBuff,
				"INFO: New Client Connected. Wait Resolution %ld msec(s). Assigning id %lu...",
				(clients.clients[clients.size].tickResMicro), clients.size);
		pthread_mutex_lock(&logFile_mutex);
		write_log(logFile, logBuff);
		pthread_mutex_unlock(&logFile_mutex);
		memset(logBuff, '\0', sizeof(char) * BUFSIZ);

		++(clients.size);

	}

	return NULL ;
}

TIMESPEC fnTimeDiffTime(const TIMESPEC* begin, const TIMESPEC* end) {
	TIMESPEC tmp;
	tmp.tv_sec = (end->tv_sec - begin->tv_sec);
	tmp.tv_nsec = (end->tv_nsec - begin->tv_nsec);

	if ((end->tv_nsec - begin->tv_nsec) < 0) {
		tmp.tv_sec -= 1;
		tmp.tv_nsec += BILLION;
	}

	return tmp;
}

long fnTimeDiffLong(const TIMESPEC* begin, const TIMESPEC* end) {
	TIMESPEC diff = fnTimeDiffTime(begin, end);
	return ((diff.tv_sec * BILLION) + diff.tv_nsec);
}

void fnSigIntHandler(int SigNum) {

	fprintf(stderr, "INFO: SIGINT caught, exiting properly...\n");
	fflush(stderr);

	write_log(logFile, "INFO: SIGINT caught, exiting properly...");

	fnExitHandler(EXIT_SIGINT);

}

void fnExitHandler(const int ExitCode) {
	size_t i = 0;
	int sigTimerDead = SERVER_DIE;

	loopCond = 0;

	pthread_cancel(threadSockListener);

	write_log(logFile, "INFO: Socket listener closed...");

	close(sockListen); /* close listen socket for everything */

	write_log(logFile, "INFO: Listen Socket closed...");

	/* TODO log "sending die signal to clients */
	for (i = 0; i < clients.size; ++i) {
		send(clients.clients[i].sock, &sigTimerDead, sizeof(int), 0);
	}

	write_log(logFile, "INFO: Server Dead Signal sent to clients...");

	sleep(1);

	/* TODO log "close all client sockets" */
	for (i = 0; i < clients.size; ++i) {
		close(clients.clients[i].sock);
	}

	write_log(logFile, "INFO: Client Sockets closed...");

	pthread_mutex_destroy(&logFile_mutex);

	write_log(logFile, "INFO: Exit Sequences done...");
	fclose(logFile);

	exit(ExitCode);
}
