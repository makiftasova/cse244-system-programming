/*
 * mathserver.h
 *
 * CSE244 - Project 2 - Math Server
 *
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 */

#ifndef MAT_SERVER_H_
#define MAT_SERVER_H_

/*
 * Exit codes
 */

#define EXIT_ILLEGAL_FORMAT 50

/*
 * Prints usage. Then returns exit code.
 *
 * execName - Current executable name of program.
 */
void fnUsage(CHAR_PTR execName);

/*
 * SIGINT handler
 */
void fnSigIntHandler(int SigNum);

/*
 * Exit handler.
 */
void fnExitHandler(const int ExitCode);

#endif /* MAT_SERVER_H_ */
