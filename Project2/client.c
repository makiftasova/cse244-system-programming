/*
 * client.c
 *
 * CSE244 - Project 2 - Client
 *
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 */

#include "common.h"
#include "client.h"

volatile SOCKET_DES sockTimer;
volatile SOCKET_DES sockMath;

volatile int TickServerId;
volatile int MathServerId;

volatile int loopCond; /* genereic infinite loop contidion */

MATRIX_T matrix1;
MATRIX_T matrix2;

volatile long numOfTicks; /* num of received ticks */

FILE_PTR logFile;

pthread_mutex_t logFile_mutex = PTHREAD_MUTEX_INITIALIZER;

int main(int argc, CHAR_PTR argv[]) {

	/*
	 * Here comes variables
	 */
	SOCK_CLIENT_ADDR destAddr;
	size_t bytes_recv = 0;
	int tickId = 0;
	FILE_PTR matrixFile;

	long timeRes;
	char serverIP[IP_LEN];
	char fileName[LINE_MAX];

	int i = 0, j = 0;

	char logBuff[BUFSIZ];

	memset(logBuff, '\0', sizeof(char) * BUFSIZ);
	memset(serverIP, '\0', sizeof(char) * IP_LEN);
	memset(fileName, '\0', sizeof(char) * LINE_MAX);

	sprintf(logBuff, CLIENT_LOG_FILE, getpid());

	logFile = fopen(logBuff, "w");

	memset(logBuff, '\0', sizeof(char) * BUFSIZ);

	loopCond = 1;
	numOfTicks = 0;

	write_log(logFile, "INFO: Installing SIGHANDLERs.");

	/* apply SIGINT handler */
	if (signal(SIGINT, fnSigIntHandler) == SIG_ERR ) {
		return (EXIT_SIGHANDLER_FAIL);
	}
	/* ignore SIGPIPE */
	if (signal(SIGPIPE, SIG_IGN ) == SIG_ERR ) {
		return (EXIT_SIGHANDLER_FAIL);
	}

	memcpy(serverIP, LOCALHOST, sizeof(char) * strlen(LOCALHOST));

	switch (argc) {
	case 4:
		memcpy(serverIP, argv[3], sizeof(char) * strlen(argv[3]));
		/* no break */
	case 3:
		timeRes = atol(argv[1]);
		if (0 >= timeRes) {
			printf("Illegal Format for Resolution: %s\n", argv[1]);
			fnUsage(argv[0]);
			return (EXIT_ILLEGAL_RESOLUTION);
		}
		memcpy(fileName, argv[2], sizeof(char) * strlen(argv[2]));
		sprintf(logBuff,
				"INFO: Server IP: %s, Time Resolution: %ld msec(s), Matrix File: %s",
				serverIP, timeRes, fileName);
		write_log(logFile, logBuff);
		memset(logBuff, '\0', sizeof(char) * BUFSIZ);
		break;
	default:
		fnUsage(argv[0]);
		return (EXIT_USAGE);
	}

	if (access(fileName, F_OK | R_OK) == -1) {
		sprintf(logBuff, "ERR: Failed to access file: %s", fileName);
		write_log(logFile, logBuff);
		memset(logBuff, '\0', sizeof(char) * BUFSIZ);
		return (EXIT_FILE_ACCESS_ERR);
	}

	matrixFile = fopen(fileName, "r");
	fnMatrixReader(matrixFile, &matrix1, &matrix2);
	fclose(matrixFile);

	write_log(logFile, "INFO: Matrix Read Success...");

	memset(logBuff, '\0', sizeof(char) * BUFSIZ);
	sprintf(logBuff, "Matrix A:\n---------\n");

	for (i = 0; i < matrix1.height; ++i) {
		for (j = 0; j < matrix1.width; ++j) {
			sprintf(logBuff, "%s%f  ", logBuff, matrix1.matrix[i][j]);
		}
		sprintf(logBuff, "%s\n", logBuff);
	}
	sprintf(logBuff, "%s\n\nMatrix B:\n---------\n", logBuff);

	for (i = 0; i < matrix2.height; ++i) {
		for (j = 0; j < matrix2.width; ++j) {
			sprintf(logBuff, "%s%f  ", logBuff, matrix2.matrix[i][j]);
		}
		sprintf(logBuff, "%s\n", logBuff);
	}

	fprintf(logFile, logBuff);
	memset(logBuff, '\0', sizeof(char) * BUFSIZ);
	fprintf(logFile, "\nEnd of Matrixes...\n\n");

	sockTimer = socket(AF_INET, SOCK_STREAM, 0);

	write_log(logFile, "INFO: Connecting socket");

	if (sockTimer < 0) {
		perror("Failed to connect socket");
		write_log(logFile, "ERR: Failed to connect Socket.");
		exit(EXIT_SOCK_CONN_FAIL);
	}

	destAddr.sin_family = AF_INET;
	destAddr.sin_port = htons(TIMER_PORT);
	destAddr.sin_addr.s_addr = inet_addr(serverIP);
	memset(&(destAddr.sin_zero), '0', 8);

	if (connect(sockTimer, (SOCK_SERVER_ADDR*) &destAddr,
			sizeof(SOCK_SERVER_ADDR)) == -1) {
		perror("connect fail");
		exit(-1);
	}

	recv(sockTimer, &tickId, sizeof(int), 0);
	TickServerId = tickId - 2; /* Id for tickServer */

	send(sockTimer, &timeRes, sizeof(long), 0); /* send time resolution to tickServer */

	sprintf(logBuff, "INFO: TickServer client ID: %d", TickServerId);
	write_log(logFile, logBuff);
	memset(logBuff, '\0', sizeof(char) * BUFSIZ);

	while (loopCond) {
		bytes_recv = recv(sockTimer, &tickId, sizeof(int), 0);
		if (bytes_recv == 0) {
			perror("sock close");
			write_log(logFile,
					"INFO: TickServer looks unreachable, Exiting...");
			fnExitHandler(EXIT_TICK_CONN_LOST);
		} else if (tickId == SERVER_TICK) {
			/* TODO do job */
			++numOfTicks;
		} else if (tickId == SERVER_DIE) {
			printf("INFO: TickServer is dead, Exiting...\n");
			write_log(logFile, "INFO: TickServer is dead, Exiting...");
			fnExitHandler(EXIT_SUCCESS);
		} else {
			printf("WARN: Unrecognized command from tick server\n");
			write_log(logFile, "WARN: Unrecognized command from tick server");
			fnExitHandler(EXIT_FAILURE);
		}
	}

	close(sockTimer);

	return (EXIT_SUCCESS);
}

void fnUsage(CHAR_PTR execName) {
	char strMsg[220];

	sprintf(strMsg,
			"%s <miliseconds> <FILENAME> <ip>\n\n<miliseconds>  Time resolution of Client server.\n<FILENAME>     Filename of matrix file.\n<ip>           IP address of Machine which runs TimerServer and MathServer. (OPTIONAL)\n",
			execName);
	write(STDOUT_FILENO, strMsg, (sizeof(char) * strlen(strMsg)));
}

void fnMatrixReader(FILE_PTR file, MATRIX_T* mx1, MATRIX_T* mx2) {
	char lineBuf[LINE_MAX], temp[20];
	char tmpChar;
	int i = 0, j = 0;
	float tmpFloat = 0.0f;

	memset(lineBuf, '\0', sizeof(char) * LINE_MAX);
	memset(temp, '\0', sizeof(char) * 20);

	while (fscanf(file, "%c", &tmpChar)) {
		if (tmpChar == '[')
			break;
	}
	while (fscanf(file, "%c", &tmpChar)) {
		if (tmpChar == '\n') {
			break;
		} else if (tmpChar == ' ') {
			++i;
		}
	}

	if ((i > MATRIX_LEN)) {
		write_log(logFile,
				"ERR: Matrix is oversized (maximum an 6*6 matrix is supported.");
		fnExitHandler(EXIT_MATRIX_OVERSIZE);
	}

	mx1->width = i;
	mx1->height = i;
	mx2->width = i;
	mx2->height = 1; /* this will always be 1 (one) */

	rewind(file);

	while (fscanf(file, "%c", &tmpChar)) {
		if (tmpChar == '[')
			break;
	}

	i = 0;
	j = 0;

	/* Matrix 1 */
	do {
		fscanf(file, "%s", temp);

		if (temp[0] == ']') {

			break;
		} else {
			tmpFloat = atof(temp);
			mx1->matrix[i][j] = tmpFloat;
			++j;
		}

		if ((j % mx1->width) == 0) {
			++i;
			j = 0;
		}

	} while (1);

	/* Matrix 2 */
	while (fscanf(file, "%c", &tmpChar)) {
		if (tmpChar == '[')
			break;
	}

	i = 0;
	j = 0;
	do {
		fscanf(file, "%s", temp);

		if (temp[0] == ']') {

			break;
		} else {
			tmpFloat = atof(temp);
			mx2->matrix[i][j] = tmpFloat;
			++j;
		}

		if ((j % mx2->width) == 0) {
			++i;
			j = 0;
		}

	} while (1);
}

void fnSigIntHandler(int SigNum) {

	fprintf(stderr, "INFO: SIGINT caught, exiting properly...\n");
	fflush(stderr);

	write_log(logFile, "INFO: SIGINT caught, exiting properly...");

	fnExitHandler(EXIT_SIGINT);

}

void fnExitHandler(const int ExitCode) {

	char logMsg[128];
	memset(logMsg, '\0', sizeof(char) * 128);

	loopCond = 0;

	close(sockTimer);
	close(sockMath);

	write_log(logFile, "INFO: Sockets closed...");

	sprintf(logMsg, "INFO: Killed after #%ld ticks", numOfTicks);
	write_log(logFile, logMsg);
	memset(logMsg, '\0', sizeof(char) * 128);

	fclose(logFile);

	exit(ExitCode);
}
