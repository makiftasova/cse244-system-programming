/*
 * mathserver.c
 *
 * CSE244 - Project 2 - Math Server
 *
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 */

#include "common.h"
#include "mathserver.h"

volatile SOCKET_DES sockTimer;
volatile SOCKET_DES sockListen;

volatile int TickServerId; /* client id for tickserver */

volatile int loopCond; /* generic infinite loop condition */

volatile long numOfTicks; /* num of received ticks */

int maxClients;

int main(int argc, CHAR_PTR argv[]) {

	/*
	 * Here comes variables
	 */
	SOCK_CLIENT_ADDR destAddr;
	size_t bytes_recv = 0;
	int tickId = 0;
	long timeRes;

	loopCond = 1;

	/* apply SIGINT handler */
	if (signal(SIGINT, fnSigIntHandler) == SIG_ERR ) {
		return (EXIT_SIGHANDLER_FAIL);
	}
	/* ignore SIGPIPE */
	if (signal(SIGPIPE, SIG_IGN ) == SIG_ERR ) {
		return (EXIT_SIGHANDLER_FAIL);
	}

	switch (argc) {
	case 3:
		timeRes = atol(argv[1]);
		if (0 >= timeRes) {
			printf("Illegal Format for Resolution: %s\n", argv[1]);
			fnUsage(argv[0]);
			return (EXIT_ILLEGAL_FORMAT);
		}
		maxClients = atoi(argv[2]);
		if (0 >= maxClients) {
			printf("Illegal Format for Max # of Clients: %s\n", argv[1]);
			fnUsage(argv[0]);
			return (EXIT_ILLEGAL_FORMAT);
		}
		break;
	default:
		fnUsage(argv[0]);
		return (EXIT_USAGE);
	}

	sockTimer = socket(AF_INET, SOCK_STREAM, 0);

	if (sockTimer < 0) {
		perror("Failed to connect socket"); /* TODO log */
		exit(EXIT_SOCK_CONN_FAIL);
	}

	destAddr.sin_family = AF_INET;
	destAddr.sin_port = htons(TIMER_PORT);
	destAddr.sin_addr.s_addr = inet_addr(LOCALHOST);
	memset(&(destAddr.sin_zero), '0', 8);

	if (connect(sockTimer, (SOCK_SERVER_ADDR*) &destAddr,
			sizeof(SOCK_SERVER_ADDR)) == -1) {
		perror("connect fail");
		exit(-1);
	}

	recv(sockTimer, &tickId, sizeof(int), 0);
	TickServerId = tickId - 2; /* Id for tickServer */

	send(sockTimer, &timeRes, sizeof(long), 0); /* send time resolution to tickServer */

	printf("id: %d\n", TickServerId);

	while (loopCond) {
		bytes_recv = recv(sockTimer, &tickId, sizeof(int), 0);
		if (bytes_recv == 0) {
			perror("sock close");
			fnExitHandler(EXIT_TICK_CONN_LOST);
		} else if (tickId == SERVER_TICK) {
			/* TODO do job */
			++numOfTicks;
		} else if (tickId == SERVER_DIE) {
			printf("tick server is K.I.A.\n");
			fnExitHandler(EXIT_SUCCESS);
		} else {
			printf("unrecognized command from tick server\n");
			fnExitHandler(EXIT_FAILURE);
		}
	}

	close(sockTimer);

	return (EXIT_SUCCESS);
}

void fnUsage(CHAR_PTR execName) {
	char strMsg[150];

	sprintf(strMsg,
			"%s <miliseconds> <max # of clients>\n\n<miliseconds>       Time resolution of Client server.\n<max # of clients>  Maximum number of clients.\n",
			execName);
	write(STDOUT_FILENO, strMsg, (sizeof(char) * strlen(strMsg)));
#ifdef VERBOSE
	printf("len:%d\n", strlen(strMsg));
#endif
}

void fnSigIntHandler(int SigNum) {

	fprintf(stderr, "INFO: SIGINT caught, exiting properly...\n");
	fflush(stderr);

	/* TODO log */

	fnExitHandler(EXIT_SIGINT);

}

void fnExitHandler(const int ExitCode) {
	loopCond = 0;

	close(sockTimer);
	close(sockListen);

	exit(ExitCode);
}
