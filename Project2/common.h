/*
 * common.h
 *
 * CSE244 - Project 2 - Common Functions
 *
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 */

#ifndef MAT_COMMON_H_
#define MAT_COMMON_H_

#include "types.h"

#define EXIT_TICK_CONN_LOST 100

#define TICKER_LOG_FILE "tickserver.log"
#define CLIENT_LOG_FILE "client-%d.log" /* "client-getpid().log"*/
#define MATH_LOG_FILE "mathserver.log"

/*
 * Add a log entry to given logFile. returns number of bytes printed.
 */
int write_log(FILE_PTR logFile, const CHAR_PTR message);

/*
 * Returns index of character ch in string str.
 */
size_t indexOf(const CHAR_PTR str, const char ch);

#endif /* MAT_COMMON_H_ */
