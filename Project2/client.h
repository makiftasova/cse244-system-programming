/*
 * client.h
 *
 * CSE244 - Project 2 - Client
 *
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 */

#ifndef MAT_CLIENT_H_
#define MAT_CLIENT_H_

/*
 * Exit codes
 */

#define EXIT_MATRIX_OVERSIZE 30

#define EXIT_MATRIX_ILLEGAL 31

#define EXIT_ILLEGAL_RESOLUTION 50

#define EXIT_FILE_ACCESS_ERR 80
/*
 * Prints usage. Then returns exit code.
 *
 * execName - Current executable name of program.
 */
void fnUsage(CHAR_PTR execName);

/*
 * Reads matrices from given file
 */
void fnMatrixReader(FILE_PTR file, MATRIX_T* mx1, MATRIX_T* mx2);

/*
 * SIGINT handler
 */
void fnSigIntHandler(int SigNum);

/*
 * Exit handler.
 */
void fnExitHandler(const int ExitCode);

#endif /* MAT_CLIENT_H_ */
