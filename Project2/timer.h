/*
 * timer.h
 *
 * CSE244 - Project 2 - Tick Server
 *
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 */

#ifndef MAT_TIMER_H_
#define MAT_TIMER_H_

#include <time.h>

/*
 * Exit codes
 */

#define EXIT_ILLEGAL_RESOLUTION 50

typedef struct {
	SOCKET_DES sock; /* socket to send tick */
	SOCK_CLIENT_ADDR* client_addr; /* info of client */
	socklen_t sin_size;
	long tickResMicro;
	TIMESPEC lastTick;
	int online;
} TICKDATA_T;

typedef struct {
	size_t size;
	TICKDATA_T clients[MAX_CLIENTS_SUPPORT];
} TIMER_CLIENTS_T;

/*
 * Prints usage. Then returns exit code.
 *
 * execName - Current executable name of program.
 */
void fnUsage(CHAR_PTR execName);

/*
 * adds given client to clients list.
 */
int fnAddClient(TIMER_CLIENTS_T* clients, const TICKDATA_T client);

/*
 * Listens for client connections, and replies it.
 */
VOID_PTR fnClientListener(VOID_PTR args);

/*
 * Helper Function for fnTimeDiffLong(TIMESPEC*, TIMESPEC* )
 */
TIMESPEC fnTimeDiffTime(const TIMESPEC* begin, const TIMESPEC* end);

/*
 * Calculates time diff between two TIMESPEC
 */
long fnTimeDiffLong(const TIMESPEC* begin, const TIMESPEC* end);

/*
 * SIGINT handler
 */
void fnSigIntHandler(int SigNum);

/*
 * Exit Handler function
 */
void fnExitHandler(const int ExitCode);

#endif /* MAT_TIMER_H_ */
